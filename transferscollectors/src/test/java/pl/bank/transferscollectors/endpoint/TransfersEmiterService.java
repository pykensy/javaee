package pl.bank.transferscollectors.endpoint;

import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.*;
import pl.bank.shared.domain.model.transfer.StoreTransfers;
import pl.bank.shared.generators.mock.AccountsGenerator;
import pl.bank.shared.generators.mock.BeneficiariesGenerator;
import pl.bank.shared.generators.mock.TransfersGenerator;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.soap.*;
import java.io.Serializable;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static pl.bank.config.api.ApplicationConfig.BANK_SOAP_ACTION;
import static pl.bank.config.api.ApplicationConfig.BANK_SOAP_ENDPOINT_URL;

@Stateless
public class TransfersEmiterService implements Serializable {

    @Inject
    private Logger logger;

    @Inject
    private GenericDao dao;

    private String bankSoapAction = BANK_SOAP_ACTION;
    private String bankSoapEndpointUrl = BANK_SOAP_ENDPOINT_URL;

    public Set<Transfer> getTransfers(TransferType.Types type) {
        return getMockedTransfers(type);
    }

    private Set<Transfer> getMockedTransfers(TransferType.Types type) {
        Set<Transfer> transfers = new HashSet<>();

        int numberOfMockedTransfers = 100;

        TransferType transferType = new TransferType(UUID.randomUUID(), type.getValue(), null, null);

        final Random random = new Random();

        Set<Account> accounts = dao.findAll(Account.class);

        for (int i = 0; i < numberOfMockedTransfers; i++) {
            Transfer transfer = TransfersGenerator.generateTransfer(Transfer.TransferDirect.OUTGOING);
            transfer.setType(transferType);
            transfer.setTransactionDate(LocalDate.now());

            Account account = accounts.stream().skip(Math.abs(random.nextInt()) % accounts.size()).findFirst().get();

            Beneficiary beneficiary = BeneficiariesGenerator.generateBeneficiary();
            beneficiary.setAccountIban(account.getIban());
            beneficiary.setTransfer(transfer);
            transfer.setBeneficiary(beneficiary);
            User user = account.getUser();
            account.setUser(user);
            transfer.setAccount(account);

            transfers.add(transfer);
        }

        return transfers;
    }

    public boolean sendSOAP(Set<Transfer> transfersToEmit) {
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(bankSoapAction, transfersToEmit), bankSoapEndpointUrl);

            soapResponse.writeTo(System.out);
            Document document = soapResponse.getSOAPBody().extractContentAsDocument();
            Node firstChild = document.getDocumentElement().getFirstChild().getFirstChild();
            String value = firstChild.getNodeValue();
            logger.info(value);

            soapConnection.close();
            return Boolean.valueOf(value);
        } catch (Exception e) {
            logger.error("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return false;
    }

    private SOAPMessage createSOAPRequest(String soapAction, Set<Transfer> transfers) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, transfers);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();
        logger.info(soapMessage.toString());

        logger.info("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }

    private void createSoapEnvelope(SOAPMessage soapMessage, Set<Transfer> transfers) throws SOAPException, NoSuchFieldException, InstantiationException, IllegalAccessException, JAXBException {
        soapMessage.setProperty(SOAPMessage.WRITE_XML_DECLARATION, "true");
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "ns2";
        String myNamespaceURI = "http://endpoint.transferscollectors.bank.pl/";

        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.removeNamespaceDeclaration("SOAP-ENV");
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
        envelope.setPrefix("S");

        envelope.getHeader().removeNamespaceDeclaration("SOAP-ENV");

        SOAPBody soapBody = envelope.getBody();
        soapBody.setPrefix("S");
        soapBody.addNamespaceDeclaration(myNamespace, myNamespaceURI);

        String transfersXml = getMarshalledTransfers(transfers);
        transfersXml = transfersXml.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");

        SOAPElement soapBodyElem = soapBody.addChildElement("collectTransfers", "ns2");
        SOAPElement arg0 = soapBodyElem.addChildElement("arg0");

        arg0.addTextNode(transfersXml);
        soapBodyElem.addChildElement(arg0);
    }

    private String getMarshalledTransfers(Set<Transfer> transfers) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(StoreTransfers.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(new StoreTransfers(transfers), stringWriter);

        return stringWriter.toString();
    }
}
