package pl.bank.transferscollectors.endpoint;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.transferscollectors.service.TransfersCollectorsService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.ws.Endpoint;

import java.lang.reflect.Field;
import java.util.Set;

import static org.junit.Assert.*;
import static pl.bank.config.api.ApplicationConfig.BANK_SOAP_ENDPOINT_URL;
import static pl.bank.config.api.ApplicationConfig.KIR_SOAP_ENDPOINT_URL;

@RunWith(Arquillian.class)
public class TransfersCollectorImplTest {

    private TransfersEmiterService transfersEmiterService;

    private Endpoint endpoint;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, Filters.exclude("pl.bank.kir.emit"), "pl.bank")
                .addAsResource("META-INF/beans.xml");
    }

    @Before
    public void setUp() throws Exception {
        TransfersCollectorImpl transfersCollectorImpl = new TransfersCollectorImpl();
        Field transfersCollectorImplLogger = TransfersCollectorImpl.class.getDeclaredField("logger");
        transfersCollectorImplLogger.setAccessible(true);
        transfersCollectorImplLogger.set(transfersCollectorImpl, LoggerFactory.getLogger(TransfersCollectorImpl.class));

        TransfersCollectorsService service = new TransfersCollectorsService();

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("h2");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        GenericDao dao = new GenericDao();
        Field emDaoField = GenericDao.class.getDeclaredField("entityManager");
        emDaoField.setAccessible(true);
        emDaoField.set(dao, entityManager);

        Field daoField = TransfersCollectorsService.class.getDeclaredField("dao");
        daoField.setAccessible(true);
        daoField.set(service, dao);

        Field serviceField = TransfersCollectorImpl.class.getDeclaredField("service");
        serviceField.setAccessible(true);
        serviceField.set(transfersCollectorImpl, service);

        transfersEmiterService = new TransfersEmiterService();
        Field emLoggerField = TransfersEmiterService.class.getDeclaredField("logger");
        emLoggerField.setAccessible(true);
        emLoggerField.set(transfersEmiterService, LoggerFactory.getLogger(TransfersEmiterService.class));

        Field daoEmiterField = TransfersEmiterService.class.getDeclaredField("dao");
        daoEmiterField.setAccessible(true);
        daoEmiterField.set(transfersEmiterService, dao);

        endpoint = Endpoint.publish(BANK_SOAP_ENDPOINT_URL, transfersCollectorImpl);
    }

    @Test
    public void collectTransfersElixirs() {
        Set<Transfer> transfers = transfersEmiterService.getTransfers(TransferType.Types.ELIXIR);
        Assert.assertTrue(transfersEmiterService.sendSOAP(transfers));
    }

    @Test
    public void collectTransfersExpressElixirs() {
        Set<Transfer> transfers = transfersEmiterService.getTransfers(TransferType.Types.EXPRESS_ELIXIR);
        Assert.assertTrue(transfersEmiterService.sendSOAP(transfers));
    }

    @Test
    public void collectTransfersSorbnets() {
        Set<Transfer> transfers = transfersEmiterService.getTransfers(TransferType.Types.SORBNET);
        Assert.assertTrue(transfersEmiterService.sendSOAP(transfers));
    }

    @After
    public void tearDown() {
        endpoint.stop();
    }
}
