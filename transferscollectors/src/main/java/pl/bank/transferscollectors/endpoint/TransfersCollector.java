package pl.bank.transferscollectors.endpoint;


import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface TransfersCollector {

    @WebMethod
    public boolean collectTransfers(String storeTransfersXml);
}
