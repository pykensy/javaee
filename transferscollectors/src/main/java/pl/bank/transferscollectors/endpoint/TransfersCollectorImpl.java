package pl.bank.transferscollectors.endpoint;

import org.slf4j.Logger;
import pl.bank.shared.domain.model.transfer.StoreTransfers;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.util.converter.XmlDeserializer;
import pl.bank.transferscollectors.service.TransfersCollectorsService;

import javax.inject.Inject;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Set;

@WebService(
        endpointInterface = "pl.bank.transferscollectors.endpoint.TransfersCollector",
        serviceName = "TransfersCollectorImpl"
)
public class TransfersCollectorImpl implements TransfersCollector {

    @Inject
    private Logger logger;

    @Inject
    private TransfersCollectorsService service;

    @Override
    public boolean collectTransfers(String storeTransfersXml) {
        StoreTransfers storeTransfers = (StoreTransfers) XmlDeserializer.deserialize(storeTransfersXml, StoreTransfers.class);
        logger.info(storeTransfers.getTransfers().toString());
        Set<Transfer> preparedTransfers = service.prepareTransfers(storeTransfers.getTransfers());
        service.updateAccounts(preparedTransfers);
        return storeTransfers.getTransfers().size() > 0 && service.saveTransfers(preparedTransfers);
    }

}
