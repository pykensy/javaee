package pl.bank.transferscollectors.service;

import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Account;
import pl.bank.shared.domain.model.Beneficiary;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Stateless
public class TransfersCollectorsService {

    @Inject
    private GenericDao dao;

    public boolean saveTransfers(Set<Transfer> transfers) {
        transfers.forEach(dao::merge);
        return true;
    }

    public Set<Transfer> prepareTransfers(Set<Transfer> transfers) {
        Set<Transfer> preparedTransfers = new HashSet<>();

        transfers.forEach(rawTransfer -> {
            Transfer transfer = new Transfer();
            transfer.setId(UUID.randomUUID());
            transfer.setTransactionDate(rawTransfer.getTransactionDate());
            transfer.setTitle(rawTransfer.getTitle());
            transfer.setAmount(rawTransfer.getAmount());
            transfer.setStatus(Transfer.TransferStatuses.ACCEPTED.getValue());
            transfer.setDirect(Transfer.TransferDirect.INCOMING.getValue());
            transfer.setType(dao.findByProperty(TransferType.class, "name", rawTransfer.getType().getName()).stream().findFirst().get());

            Beneficiary beneficiary = new Beneficiary();
            beneficiary.setId(UUID.randomUUID());
            beneficiary.setAccountIban(rawTransfer.getAccount().getIban());
            beneficiary.setFullName(rawTransfer.getAccount().getUser().getFirstName() + " " + rawTransfer.getAccount().getUser().getLastName());
            transfer.setBeneficiary(beneficiary);

            Account account = dao.findByProperty(Account.class, "iban", rawTransfer.getBeneficiary().getAccountIban()).stream().findFirst().get();
            transfer.setAccount(account);

            preparedTransfers.add(transfer);
        });

        return preparedTransfers;
    }

    public void updateAccounts(Set<Transfer> preparedTransfers) {
        preparedTransfers.forEach(transfer -> {
            Account account = transfer.getAccount();
            double newBalance = account.getBalance() + transfer.getAmount();
            account.setBalance(newBalance);
        });
    }
}
