package pl.bank.kir.emit.service;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bank.config.MockResource;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.transferscollectors.endpoint.TransfersCollectorImpl;
import pl.bank.transferscollectors.service.TransfersCollectorsService;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.ws.Endpoint;

import java.lang.reflect.Field;
import java.util.Set;

import static org.junit.Assert.*;
import static pl.bank.config.api.ApplicationConfig.BANK_SOAP_ENDPOINT_URL;

@RunWith(Arquillian.class)
public class TransfersEmiterServiceTest {

    @EJB
    private TransfersEmiterService transfersEmiterService;

    @Inject
    private TransfersCollectorsService service;

    @Inject
    private GenericDao dao;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, "pl.bank")
                .addAsResource("META-INF/beans.xml");
    }

    @Before
    public void setUp() throws Exception {
        MockResource mockResource = MockResource.getInstance();

        Logger logger = LoggerFactory.getLogger(TransfersCollectorImpl.class);

        TransfersCollectorImpl soapEndpoint = new TransfersCollectorImpl();

        Field loggerField = soapEndpoint.getClass().getDeclaredField("logger");
        loggerField.setAccessible(true);
        loggerField.set(soapEndpoint, logger);

        Field serviceField = soapEndpoint.getClass().getDeclaredField("service");
        serviceField.setAccessible(true);
        serviceField.set(soapEndpoint, service);


        Endpoint.publish(BANK_SOAP_ENDPOINT_URL, soapEndpoint);
    }

    @Test
    public void sendSOAP() {
        int numberOfTransfersBeforeEmit = dao.findAll(Transfer.class).size();
        Set<Transfer> transfers = transfersEmiterService.getTransfers(TransferType.Types.ELIXIR);
        boolean result = transfersEmiterService.sendSOAP(transfers);
        int numberOfTransfersAfterEmit = dao.findAll(Transfer.class).size();
        assertTrue(result);
        assertNotEquals(numberOfTransfersBeforeEmit, numberOfTransfersAfterEmit);
        assertEquals(numberOfTransfersBeforeEmit + 100, numberOfTransfersAfterEmit);
    }
}
