package pl.bank.kir.endpoint;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bank.kir.store.endpoint.KirStoreImpl;
import pl.bank.kir.store.service.KirService;
import pl.bank.shared.domain.model.Beneficiary;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.shared.generators.mock.AccountsGenerator;
import pl.bank.shared.generators.mock.BeneficiariesGenerator;
import pl.bank.shared.generators.mock.TransfersGenerator;

import javax.xml.soap.SOAPException;
import javax.xml.ws.Endpoint;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static pl.bank.config.api.ApplicationConfig.KIR_SOAP_ENDPOINT_URL;

@RunWith(Arquillian.class)
public class KirStoreImplTest {

    private Logger logger = LoggerFactory.getLogger(KirStoreImpl.class);

    private TransferOutputSoapService transferOutputSoapService;

    private Endpoint endpoint;

    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, Filters.exclude("pl.bank.kir.emit"), "pl.bank")
                .addAsResource("META-INF/beans.xml");
    }

    @Before
    public void setUp() throws Exception {
        KirStoreImpl kirStore = new KirStoreImpl();
        Field loggerField = KirStoreImpl.class.getDeclaredField("logger");
        loggerField.setAccessible(true);
        loggerField.set(kirStore, logger);

        KirService kirService = new KirService();

        Field serviceField = KirStoreImpl.class.getDeclaredField("kirService");
        serviceField.setAccessible(true);
        serviceField.set(kirStore, kirService);

        transferOutputSoapService = new TransferOutputSoapService();
        Field tissLogger = TransferOutputSoapService.class.getDeclaredField("logger");
        tissLogger.setAccessible(true);
        tissLogger.set(transferOutputSoapService, LoggerFactory.getLogger(TransferOutputSoapService.class));

        if (endpoint == null || !endpoint.isPublished()) {
            endpoint = Endpoint.publish(KIR_SOAP_ENDPOINT_URL, kirStore);
        }
    }

    @InSequence(0)
    @RunAsClient
    @Test
    public void storeElixirs() throws SOAPException {
        Set<Transfer> elixirs = generateFakeTransferOfType(TransferType.Types.ELIXIR);
//        Assert.assertTrue(transferOutputSoapService.sendTransfers(elixirs, TransferType.Types.ELIXIR));
    }

    @InSequence(1)
    @RunAsClient
    @Test
    public void storeExpressElixirs() throws SOAPException {
        Set<Transfer> expressElixirs = generateFakeTransferOfType(TransferType.Types.EXPRESS_ELIXIR);
//        Assert.assertTrue(transferOutputSoapService.sendTransfers(expressElixirs, TransferType.Types.EXPRESS_ELIXIR));
    }

    @InSequence(2)
    @RunAsClient
    @Test
    public void storeSorbnets() throws SOAPException {
        Set<Transfer> sorbnets = generateFakeTransferOfType(TransferType.Types.SORBNET);
//        Assert.assertTrue(transferOutputSoapService.sendTransfers(sorbnets, TransferType.Types.SORBNET));
    }

    private Set<Transfer> generateFakeTransferOfType(TransferType.Types type) {
        Set<Transfer> transfers = new HashSet<>();
        TransferType transferType = new TransferType(UUID.randomUUID(), type.getValue(), null, null);

        for (int i = 0; i < 100; i++) {
            Transfer transfer = TransfersGenerator.generateTransfer(Transfer.TransferDirect.OUTGOING);
            transfer.setStatus(Transfer.TransferStatuses.EXECUTED.getValue());
            transfer.setType(transferType);
            transfer.setTransactionDate(LocalDate.now());

            Beneficiary beneficiary = BeneficiariesGenerator.generateBeneficiary();
            beneficiary.setTransfer(transfer);
            transfer.setBeneficiary(beneficiary);

            transfer.setAccount(AccountsGenerator.generateAccount());

            transfers.add(transfer);
        }
        return transfers;
    }

    @After
    public void tearDown() {
        endpoint.stop();
    }

}
