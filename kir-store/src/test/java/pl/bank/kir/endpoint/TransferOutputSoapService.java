package pl.bank.kir.endpoint;

import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.shared.domain.model.transfer.StoreTransfers;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.soap.*;
import java.io.StringWriter;
import java.util.Set;

import static pl.bank.config.api.ApplicationConfig.KIR_SOAP_ACTION;
import static pl.bank.config.api.ApplicationConfig.KIR_SOAP_ENDPOINT_URL;

public class TransferOutputSoapService {

    private Logger logger;

    private String kirSoapAction = KIR_SOAP_ACTION;
    private String kirSoapEndpointUrl = KIR_SOAP_ENDPOINT_URL;

    public boolean sendTransfers(Set<Transfer> transfers, TransferType.Types type) throws SOAPException {
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(kirSoapAction, transfers, type), kirSoapEndpointUrl);

            soapResponse.writeTo(System.out);
            Document document = soapResponse.getSOAPBody().extractContentAsDocument();
            Node firstChild = document.getDocumentElement().getFirstChild().getFirstChild();
            String value = firstChild.getNodeValue();
            logger.info(value);

            soapConnection.close();
            return Boolean.valueOf(value);
        } catch (Exception e) {
            logger.error("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return false;
    }

    private SOAPMessage createSOAPRequest(String soapAction, Set<Transfer> transfers, TransferType.Types type) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, transfers, type);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();
        logger.info(soapMessage.toString());

        logger.info("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }

    private void createSoapEnvelope(SOAPMessage soapMessage, Set<Transfer> transfers, TransferType.Types type) throws SOAPException, NoSuchFieldException, InstantiationException, IllegalAccessException, JAXBException {
        soapMessage.setProperty(SOAPMessage.WRITE_XML_DECLARATION, "true");
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "ns2";
        String myNamespaceURI = "http://endpoint.store.kir.bank.pl/";

        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.removeNamespaceDeclaration("SOAP-ENV");
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
        envelope.setPrefix("S");

        envelope.getHeader().removeNamespaceDeclaration("SOAP-ENV");

        SOAPBody soapBody = envelope.getBody();
        soapBody.setPrefix("S");
        soapBody.addNamespaceDeclaration(myNamespace, myNamespaceURI);

        String transfersXml = getMarshalledTransfers(transfers, type);
        transfersXml = transfersXml.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");

        SOAPElement soapBodyElem = soapBody.addChildElement(getSoapAction(type), "ns2");
        SOAPElement arg0 = soapBodyElem.addChildElement("arg0");

        arg0.addTextNode(transfersXml);
        soapBodyElem.addChildElement(arg0);
    }

    private static String getMarshalledTransfers(Set<Transfer> transfers, TransferType.Types type) throws JAXBException, IllegalAccessException, NoSuchFieldException, InstantiationException {
        JAXBContext jaxbContext = JAXBContext.newInstance(StoreTransfers.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(new StoreTransfers(transfers), stringWriter);

        return stringWriter.toString();
    }

    private static String getSoapAction(TransferType.Types type) {
        String result = null;
        switch (type) {
            case ELIXIR:
                result = "storeElixirs";
                break;
            case SORBNET:
                result = "storeSorbnets";
                break;
            case EXPRESS_ELIXIR:
                result = "storeExpressElixirs";
                break;
        }
        return result;
    }
}
