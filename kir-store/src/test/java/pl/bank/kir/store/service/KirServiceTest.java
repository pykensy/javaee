package pl.bank.kir.store.service;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bank.shared.domain.model.Account;
import pl.bank.shared.domain.model.Beneficiary;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.shared.generators.IbanGenerator;
import pl.bank.shared.generators.mock.BeneficiariesGenerator;
import pl.bank.shared.generators.mock.TransfersGenerator;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static pl.bank.config.api.ApplicationConfig.KIR_STORE_DIR;
import static pl.bank.shared.generators.mock.MockData.*;

@RunWith(Arquillian.class)
public class KirServiceTest {

    private Set<Transfer> transfers;

    private KirService kirService;

    private Logger logger = LoggerFactory.getLogger(KirServiceTest.class);

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(pl.bank.kir.store.service.KirService.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Before
    public void setUp() {
        transfers = generateFakeTransferOfType(TransferType.Types.SORBNET);
        kirService = new KirService();
    }

    @Test
    public void save() throws IOException {
        Path dirPath = Paths.get(KIR_STORE_DIR);
        long numberOfFilesInDir = Files.list(dirPath)
                .filter(p -> p.toFile().isFile())
                .count();

        kirService.save(transfers);
        long numberOfFilesInDirAfterSave = Files.list(dirPath)
                .filter(p -> p.toFile().isFile())
                .count();

        Assert.assertEquals(numberOfFilesInDir + 1, numberOfFilesInDirAfterSave);
    }

    private Set<Transfer> generateFakeTransferOfType(TransferType.Types type) {
        Set<Transfer> transfers = new HashSet<>();
        TransferType transferType = new TransferType(UUID.randomUUID(), type.getValue(), null, null);

        for (int i = 0; i < 100; i++) {
            Transfer transfer = TransfersGenerator.generateTransfer(Transfer.TransferDirect.OUTGOING);
            transfer.setType(transferType);
            transfer.setTransactionDate(LocalDate.now());

            Beneficiary beneficiary = BeneficiariesGenerator.generateBeneficiary();
            transfer.setBeneficiary(beneficiary);
            transfer.setAccount(new Account());

            transfers.add(transfer);
        }
        return transfers;
    }
}
