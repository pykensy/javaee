package pl.bank.kir.emit.timer;

import org.slf4j.Logger;
import pl.bank.kir.emit.service.TransfersEmiterService;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.Set;

public abstract class TransfersEmiterTimer implements Serializable {

    @Inject
    private TransfersEmiterService transfersEmiterService;

    @Inject
    protected Logger logger;

    void emit(TransferType.Types type) {
        Set<Transfer> transfersToEmit = transfersEmiterService.getTransfers(type);
        boolean result = transfersEmiterService.sendSOAP(transfersToEmit);
        logger.info(Boolean.toString(result));
    }
}
