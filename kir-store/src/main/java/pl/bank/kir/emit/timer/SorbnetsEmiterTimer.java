package pl.bank.kir.emit.timer;

import pl.bank.shared.domain.model.TransferType;

import javax.ejb.*;
import javax.xml.soap.SOAPException;

@LocalBean
@Stateless
public class SorbnetsEmiterTimer extends TransfersEmiterTimer {
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedule(hour = "*", minute = "*/10")
    public void emit() {
        logger.info("SorbnetTransferExecutor running");
        super.emit(TransferType.Types.SORBNET);
    }
}
