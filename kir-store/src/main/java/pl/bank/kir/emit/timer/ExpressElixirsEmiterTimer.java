package pl.bank.kir.emit.timer;

import pl.bank.shared.domain.model.TransferType;

import javax.ejb.*;

@LocalBean
@Stateless
public class ExpressElixirsEmiterTimer extends TransfersEmiterTimer {
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedules({
            @Schedule(hour = "*")
    })
    public void emit() {
        logger.info("ExpressElixirsEmiterTimer running");
        super.emit(TransferType.Types.EXPRESS_ELIXIR);
    }
}
