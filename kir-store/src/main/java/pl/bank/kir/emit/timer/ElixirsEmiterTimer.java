package pl.bank.kir.emit.timer;

import pl.bank.shared.domain.model.TransferType;

import javax.ejb.*;
import javax.xml.soap.SOAPException;

@LocalBean
@Stateless
public class ElixirsEmiterTimer extends TransfersEmiterTimer {
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedules({
            @Schedule(dayOfWeek = "Mon,Tue,Wed,Thu,Fri", hour = "8", minute = "5"),
            @Schedule(dayOfWeek = "Mon,Tue,Wed,Thu,Fri", hour = "12", minute = "5"),
            @Schedule(dayOfWeek = "Mon,Tue,Wed,Thu,Fri", hour = "16", minute = "5")
    })
    public void emit() {
        logger.info("ElixirsEmiterTimer running");
        super.emit(TransferType.Types.ELIXIR);
    }
}
