package pl.bank.kir.store.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface KirStore {
    @WebMethod
    public boolean storeElixirs(String storeTransfersXml);

    @WebMethod
    public boolean storeExpressElixirs(String storeTransfersXml);

    @WebMethod
    public boolean storeSorbnets(String storeTransfersXml);
}
