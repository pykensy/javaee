package pl.bank.kir.store.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import pl.bank.shared.domain.model.Transfer;

import javax.ejb.Stateless;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import static pl.bank.config.api.ApplicationConfig.KIR_STORE_DIR;

@Stateless
public class KirService {

    public void save(Set<Transfer> transfers) throws IOException {
        Path filePath = Paths.get(KIR_STORE_DIR, Long.toString(System.currentTimeMillis()) + ".json");

        File transferFile = filePath.toFile();
        transferFile.createNewFile();

        ObjectMapper mapper = new ObjectMapper();
        String resultJson = mapper.writeValueAsString(transfers);

        try (FileWriter file = new FileWriter(transferFile)) {

            file.write(resultJson);
        }
    }
}
