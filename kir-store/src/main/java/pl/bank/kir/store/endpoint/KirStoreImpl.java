package pl.bank.kir.store.endpoint;

import org.slf4j.Logger;
import pl.bank.kir.store.service.KirService;
import pl.bank.shared.domain.model.transfer.StoreTransfers;

import javax.inject.Inject;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

@WebService(
        endpointInterface = "pl.bank.kir.store.endpoint.KirStore",
        serviceName = "KirStoreImplService"
)
public class KirStoreImpl implements KirStore {

    @Inject
    private Logger logger;

    @Inject
    private KirService kirService;

    @Override
    public boolean storeElixirs(String storeTransfersXml) {
        return storeTransfersXml != null && storeTransfer(storeTransfersXml);
    }

    @Override
    public boolean storeExpressElixirs(String storeTransfersXml) {
        return storeTransfersXml != null && storeTransfer(storeTransfersXml);
    }

    @Override
    public boolean storeSorbnets(String storeTransfersXml) {
        return storeTransfersXml != null && storeTransfer(storeTransfersXml);
    }

    private boolean storeTransfer(String storeTransfersXml) {
        StoreTransfers storeTransfers = deserialize(storeTransfersXml);

        logger.info(storeTransfers.getTransfers().toString());
        try {
            kirService.save(storeTransfers.getTransfers());
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.toString());
            return false;
        }
        return true;
    }

    private StoreTransfers deserialize(String storeTransfersXml) {
        StoreTransfers storeTransfers = new StoreTransfers();
        try {
            JAXBContext context = JAXBContext.newInstance(StoreTransfers.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            storeTransfers = (StoreTransfers)unmarshaller.unmarshal(new ByteArrayInputStream(storeTransfersXml.getBytes(StandardCharsets.UTF_8)));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return storeTransfers;
    }
}
