package pl.bank.config.resource;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class ResourcesTest {

    @Inject
    private EntityManager entityManager;

    @Inject
    private Logger logger;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addClass(Resources.class)
                .addAsResource("META-INF/beans.xml");
    }

    @Test
    public void exposeLogger() {
        assertEquals(logger.getName(), this.getClass().getName());
        assertNotNull(logger);
    }

    @Test
    public void createEntityManager() {
        assertNotNull(entityManager);
    }
}
