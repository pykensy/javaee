package pl.bank.config.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bank.config.MockResource;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@JMSDestinationDefinitions({
        @JMSDestinationDefinition(
                name = Resources.EMAIL_QUEUE,
                interfaceName = "javax.jms.Queue",
                destinationName = "emailQueue",
                resourceAdapter = "jmsra",
                description = "Queue for emails")
})
public class Resources {
    public static final String EMAIL_QUEUE = "java:global/jms/emailQueue";

    @Produces
    public Logger exposeLogger(InjectionPoint injectionPoint) {
        return LoggerFactory.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }

    @Produces
    public EntityManager createEntityManager() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("h2");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        MockResource.getInstance();
        return entityManager;
    }


}
