package pl.bank.config.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class ApplicationConfig extends Application {
    public final static String ADMIN_ROLE = "ADMIN";
    public final static String EMPLOYEE_ROLE = "EMPLOYEE";
    public final static String USER_ROLE = "USER";

    public final static String TRANSFER_DATE_FORMAT = "dd.MM.yyyy";

    public final static String KIR_SOAP_ENDPOINT_URL = "http://desktop-5md01gf:8080/bank/KirStoreImplService";
    public final static String KIR_SOAP_ACTION = "\"http://endpoint.store.kir.bank.pl/\"";

    public final static String BANK_SOAP_ENDPOINT_URL = "http://desktop-5md01gf:8080/bank/TransfersCollectorImpl";
    public final static String BANK_SOAP_ACTION = "\"http://endpoint.transferscollectors.bank.pl/\"";

    public final  static String KIR_STORE_DIR = "D:\\Projects\\javaee\\transfersstore";
}
