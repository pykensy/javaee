package pl.bank.config;

import pl.bank.shared.domain.model.*;
import pl.bank.shared.generators.mock.*;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

@Singleton
@Startup
public class MockResource {

    private static MockResource instance = new MockResource();

    private EntityManager entityManager;

    public final static int NUMBER_OF_ACCOUNTS_PER_USER = 5;
    public final static int NUMBER_OF_TRANSFERS_PER_ACCOUNT = 5;
    public final static int NUMBER_OF_USERS = 10;

    private final Random random = new Random();

    private Set<User> users;
    private Set<Account> accounts;

    public static MockResource getInstance() {
        return instance;
    }

    private MockResource() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("h2");
        entityManager = entityManagerFactory.createEntityManager();
        mockDatabaseData();
    }

    @PostConstruct
    public void mockDatabaseData() {
        mockTransferTypes();
        mockUsers();
        mockAccounts();
        mockTransfers();
    }

    private void mockUsers() {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setUsername("9999999");
        user.setRole("ADMIN");
        user.setFirstName("wipek");
        user.setLastName("wipek");
        user.setPassword("WZRHGrsBESr8wYFZ9sx0tPURuZgG2lmzyvWpwXPKz8U=");

        User user2 = new User();
        user2.setId(UUID.randomUUID());
        user2.setUsername("111111");
        user2.setRole("USER");
        user2.setFirstName("test");
        user2.setLastName("test");
        user2.setPassword("WZRHGrsBESr8wYFZ9sx0tPURuZgG2lmzyvWpwXPKz8U=");


        Set<User> users = UsersGenerator.generateUsers(NUMBER_OF_USERS - 2);

        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.persist(user2);
        users.forEach(entityManager::persist);
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    public void mockTransferTypes() {
        Set<TransferType> transferTypes = TransferTypesGenerator.generateTransferTypes();

        entityManager.getTransaction().begin();
        transferTypes.forEach(entityManager::persist);
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    public void mockAccounts() {
        users = new HashSet<>(entityManager.createQuery("SELECT u FROM User u").getResultList());

        Set<Account> accounts = new HashSet<>();

        users.forEach(i -> {
            for (int j = 0; j < NUMBER_OF_ACCOUNTS_PER_USER; j++) {
                Account account = AccountsGenerator.generateAccount();
                account.setUser(i);
                accounts.add(account);
            }
        });

        entityManager.getTransaction().begin();
        accounts.forEach(entityManager::persist);
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    public void mockTransfers() {
        accounts = new HashSet<>(entityManager.createQuery("SELECT a FROM Account a").getResultList());
        Set<TransferType> transferTypes = new HashSet<>(entityManager.createQuery("SELECT t From TransferType t").getResultList());

        Set<Transfer> transfers = new HashSet<>();

        accounts.forEach(i -> {
            for (int j = 0; j < NUMBER_OF_TRANSFERS_PER_ACCOUNT; j++) {
                Transfer transfer = TransfersGenerator.generateTransfer(Transfer.TransferDirect.OUTGOING);
                transfer.setAccount(i);
                transfer.setType(transferTypes.stream().skip(Math.abs(random.nextInt()) % transferTypes.size()).findFirst().get());

                Beneficiary beneficiary = mockBeneficiary();
                beneficiary.setTransfer(transfer);
                transfer.setBeneficiary(beneficiary);

                transfers.add(transfer);
            }
        });

        accounts.forEach(i -> {
            for (int j = 0; j < NUMBER_OF_TRANSFERS_PER_ACCOUNT; j++) {
                Transfer transfer = TransfersGenerator.generateTransfer(Transfer.TransferDirect.INCOMING);
                transfer.setAccount(i);
                transfer.setType(transferTypes.stream().skip(Math.abs(random.nextInt()) % transferTypes.size()).findFirst().get());

                Beneficiary beneficiary = mockBeneficiary();
                beneficiary.setTransfer(transfer);
                transfer.setBeneficiary(beneficiary);

                transfers.add(transfer);
            }
        });

        entityManager.getTransaction().begin();
        transfers.forEach(entityManager::persist);
        entityManager.flush();
        entityManager.getTransaction().commit();

    }

    public Beneficiary mockBeneficiary() {

        Beneficiary beneficiary = BeneficiariesGenerator.generateBeneficiary();

        if (random.nextBoolean()) {
            beneficiary.setAccountIban(accounts.stream().skip(Math.abs(random.nextInt()) % accounts.size()).findFirst().get().getIban());
        }

        return beneficiary;
    }

    public void generateFakeTransferOfType(EntityManager entityManager, TransferType.Types type) {
        Set<Transfer> transfers = new HashSet<>();
        TransferType transferType = (TransferType) entityManager.createQuery("SELECT t FROM TransferType t WHERE t.name = :name").setParameter("name", type.getValue()).getResultList().stream().findFirst().get();

        for (int i = 0; i < 100; i++) {
            Transfer transfer = TransfersGenerator.generateTransfer(Transfer.TransferDirect.OUTGOING);
            transfer.setStatus(Transfer.TransferStatuses.SAVED.getValue());
            transfer.setType(transferType);

            boolean whichDate = random.nextBoolean();
            LocalDate date = whichDate ? LocalDate.now() : LocalDate.now().plus(1, ChronoUnit.DAYS);
            transfer.setTransactionDate(date);

            Beneficiary beneficiary = BeneficiariesGenerator.generateBeneficiary();
            beneficiary.setTransfer(transfer);
            transfer.setBeneficiary(beneficiary);

            transfer.setAccount(accounts.stream().skip(Math.abs(random.nextInt()) % accounts.size()).findFirst().get());

            transfers.add(transfer);
        }

        entityManager.getTransaction().begin();
        transfers.forEach(entityManager::persist);
        entityManager.flush();
        entityManager.getTransaction().commit();
    }


}
