package pl.bank.shared;

import org.junit.Assert;
import org.junit.Test;
import pl.bank.shared.generators.IbanGenerator;

public class IbanGeneratorTest {

    @Test
    public void generate() {
        String iban = IbanGenerator.generate();
        System.out.println(iban);
        Assert.assertEquals(iban.length(), 28);
    }
}
