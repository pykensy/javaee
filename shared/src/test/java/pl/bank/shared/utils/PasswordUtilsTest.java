package pl.bank.shared.utils;

import org.junit.Assert;
import org.junit.Test;

public class PasswordUtilsTest {

    private final static String HASHED_12345 = "WZRHGrsBESr8wYFZ9sx0tPURuZgG2lmzyvWpwXPKz8U=";

    private String stringToHash = "12345";

    @Test
    public void digestPassword() {
        String hashResult = PasswordUtils.digestPassword(stringToHash);
        Assert.assertEquals(HASHED_12345, hashResult);
    }
}
