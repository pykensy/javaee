package pl.bank.shared.validators;

import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.validators.interfaces.ValidAccount;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Properties;

public class ValidAccountValidator implements
        ConstraintValidator<ValidAccount, String> {

    private GenericDao dao;

    public ValidAccountValidator() {
        Properties prop = new Properties();
        prop.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        try {
            Context context = new InitialContext(prop);
            this.dao = (GenericDao)context.lookup("java:global/bank/GenericDao");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
//        Account accountByIdFromRequest = dao.find(Account.class, UUID.fromString(value));
//        return accountByIdFromRequest != null;
        // todo because mocked accounts
        return true;
    }
}
