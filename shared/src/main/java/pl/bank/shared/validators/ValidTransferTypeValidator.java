package pl.bank.shared.validators;

import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.shared.validators.interfaces.ValidTransferType;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;

public class ValidTransferTypeValidator  implements
        ConstraintValidator<ValidTransferType, String> {

    private GenericDao dao;

    public ValidTransferTypeValidator() {
        Properties prop = new Properties();
        prop.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        try {
            Context context = new InitialContext(prop);
            this.dao = (GenericDao)context.lookup("java:global/bank/GenericDao");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        Optional<TransferType> transferTypeByRequestId = dao.find(TransferType.class, UUID.fromString(value));
        return transferTypeByRequestId.isPresent();
    }
}
