package pl.bank.shared.validators.interfaces;

import pl.bank.shared.validators.ValidEnoughMoneyInAccountValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = ValidEnoughMoneyInAccountValidator.class)
@Documented
@Retention(RUNTIME)
@Target({TYPE})
public @interface EnoughMoneyInAccount {
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
