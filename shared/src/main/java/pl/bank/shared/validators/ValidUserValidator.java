package pl.bank.shared.validators;

import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.User;
import pl.bank.shared.validators.interfaces.ValidUser;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Properties;
import java.util.UUID;

public class ValidUserValidator implements
        ConstraintValidator<ValidUser, String> {

    private GenericDao dao;

    public ValidUserValidator() {
        Properties prop = new Properties();
        prop.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        try {
            Context context = new InitialContext(prop);
            this.dao = (GenericDao)context.lookup("java:global/bank/GenericDao");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return dao.find(User.class, UUID.fromString(value)).isPresent();
    }
}
