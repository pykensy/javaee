package pl.bank.shared.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.persistence.annotations.UuidGenerator;
import pl.bank.shared.util.converter.UUIDAttributeConverter;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@EqualsAndHashCode(callSuper = true, exclude = {"transfer"})
@ToString(exclude = {"transfer"})
@Table(name = "Beneficiaries")
//@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beneficiary", propOrder = {
        "accountIban",
        "fullName",
        "address"
})
public class Beneficiary extends BaseEntity<UUID> {

    private static final long serialVersionUID = 1L;

    @Column(columnDefinition = "uuid")
    @Id
    @NonNull
    @Convert(converter = UUIDAttributeConverter.class)
    @UuidGenerator(name="uuid")
    @XmlTransient
    private UUID id;

    @Column
    private String accountIban;

    @Column
    private String fullName;

    @Column
    private String address;

    @XmlTransient
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSFER_ID")
    private Transfer transfer;
}


