package pl.bank.shared.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.persistence.annotations.UuidGenerator;
import pl.bank.shared.util.converter.UUIDAttributeConverter;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

//import org.eclipse.persistence.annotations.Convert;
//import org.eclipse.persistence.annotations.Converter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@EqualsAndHashCode(callSuper = true, exclude = {"accounts"})
@ToString(exclude = {"accounts"})
@Table(name = "Users")
@NamedQuery(
        name=User.FIND_USER_BY_USERNAME_AND_PASSWORD,
        query="SELECT u FROM User u WHERE u.username = :username AND u.password = :password"
)
//@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "user", propOrder = {
        "firstName",
        "lastName"
})
public class User extends BaseEntity<UUID> {
    private static final long serialVersionUID = 1L;

    public static final String FIND_USER_BY_USERNAME_AND_PASSWORD = "findUserByUsernameAndPassword";

    @Column(columnDefinition = "uuid")
    @Id
    @Convert(converter = UUIDAttributeConverter.class)
    @UuidGenerator(name="uuid")
    @XmlTransient
    private UUID id;

    @XmlTransient
    @Column(unique = true)
    private String username = "";

    @XmlTransient
    @Column
    private String password = "";

    @Column
    private String firstName;

    @Column
    private String lastName;

    @XmlTransient
    @Column
    private String role = "";

    @XmlTransient
    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Set<Account> accounts = new HashSet<>();
}
