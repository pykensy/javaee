package pl.bank.shared.domain.model.transfer;

import lombok.*;
import pl.bank.shared.validators.interfaces.EnoughMoneyInAccount;
import pl.bank.shared.validators.interfaces.ValidAccount;
import pl.bank.shared.validators.interfaces.ValidTransferType;
import pl.bank.shared.validators.interfaces.ValidUser;

import javax.validation.constraints.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@EnoughMoneyInAccount
public class TransferRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @ValidUser
    @NotNull
    private String userId;

    @ValidAccount
    @NotNull
    private String accountId;

    @NotNull
    private String beneficiaryName;

    @NotNull
    private String beneficiaryAccountIban;

    private String beneficiaryAddress;

    @ValidTransferType
    @NotNull
    private String transferTypeId;

    @NotNull
    @DecimalMin(value = "1.00")
    @DecimalMax(value = "999999999999.99")
    private Double amount;

    @NotNull
    private String title;

//    @FutureOrPresent
    private String date;

    @NotNull
    private boolean emailConfirm;

    @Email
    private String email;

    @Size(min = 8, max = 8)
    private String token;
}
