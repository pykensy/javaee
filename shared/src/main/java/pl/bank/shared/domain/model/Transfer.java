package pl.bank.shared.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.persistence.annotations.UuidGenerator;
import pl.bank.shared.util.converter.LocalDateAdapter;
import pl.bank.shared.util.converter.UUIDAttributeConverter;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.UUID;

import static pl.bank.shared.domain.model.Transfer.GET_ACCOUNTS_TRANSFERS_HISTORY;
import static pl.bank.shared.domain.model.Transfer.GET_TRANSFERS_TO_EXECUTE;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@EqualsAndHashCode(callSuper = true, exclude = {"beneficiary", "account", "type"})
@ToString(exclude = {"account", "beneficiary", "type"})
@Table(name = "Transfers")
@NamedQueries({
        @NamedQuery(
                name = GET_ACCOUNTS_TRANSFERS_HISTORY,
                query = "SELECT t FROM Transfer t WHERE t.account.id = :id AND t.direct = :direct"
        ),
        @NamedQuery(
                name = GET_TRANSFERS_TO_EXECUTE,
                query = "SELECT t FROM Transfer t WHERE t.status = :status AND t.type.name = :transferType"
        )
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transfer", propOrder = {
        "account",
        "amount",
        "beneficiary",
        "title",
        "transactionDate",
        "type"
})
public class Transfer extends BaseEntity<UUID> {
    private static final long serialVersionUID = 1L;

    public final static String GET_ACCOUNTS_TRANSFERS_HISTORY = "getAccountsTransfersHistory";
    public final static String GET_TRANSFERS_TO_EXECUTE = "getTransfersToExecute";

    public enum TransferStatuses {
        CREATED(0),
        EXECUTED(1),
        CANCELLED(2),
        SAVED(3),
        ACCEPTED(4);

        @Getter
        private final int value;

        TransferStatuses(int value) {
            this.value = value;
        }
    }

    public enum TransferDirect {
        OUTGOING(0),
        INCOMING(1);

        @Getter
        private final int value;

        TransferDirect(int value) {
            this.value = value;
        }
    }

    @Column(columnDefinition = "uuid")
    @Id
    @Convert(converter = UUIDAttributeConverter.class)
    @UuidGenerator(name = "uuid")
    @XmlTransient
    private UUID id;

    @Column
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    private LocalDate transactionDate = LocalDate.now();

    @Column
    private String title;

    @Column
    private Double amount;

    @XmlTransient
    @Column
    private int status;

    @XmlTransient
    private int direct = 0;

    @ManyToOne
    @JoinColumn(name = "TRANSFER_TYPE_ID")
    private TransferType type;

    @OneToOne(mappedBy = "transfer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Beneficiary beneficiary;

    @XmlElement(name = "account")
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;
}
