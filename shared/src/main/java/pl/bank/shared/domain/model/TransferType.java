package pl.bank.shared.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.persistence.annotations.UuidGenerator;
import pl.bank.shared.util.converter.UUIDAttributeConverter;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

//import org.eclipse.persistence.annotations.Convert;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@EqualsAndHashCode(callSuper = true, exclude = {"transfers"})
@ToString(exclude = {"transfers"})
@Table(name = "TransferTypes")
//@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferType", propOrder = {
        "name"
})
public class TransferType extends BaseEntity<UUID> {
    private static final long serialVersionUID = 1L;

    public final static String ELIXIR_NAME = "Elixir";
    public final static String EXPRESS_ELIXIR_NAME = "Express Elixir";
    public final static String SORBNET_NAME = "SORBNET";

    public enum Types {
        ELIXIR(ELIXIR_NAME),
        EXPRESS_ELIXIR(EXPRESS_ELIXIR_NAME),
        SORBNET(SORBNET_NAME);

        @Getter
        private String value;

        Types(String value) {
            this.value = value;
        }
    }

    @Column(columnDefinition = "uuid")
    @Id
    @Convert(converter = UUIDAttributeConverter.class)
    @UuidGenerator(name = "uuid")
    @XmlTransient
    private UUID id;

    @Column(unique = true)
    private String name;

    @XmlTransient
    @Column
    private Double price;

    @XmlTransient
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "type", cascade = CascadeType.MERGE)
    private Set<Transfer> transfers = new HashSet<>();
}
