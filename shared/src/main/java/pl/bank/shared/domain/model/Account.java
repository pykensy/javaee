package pl.bank.shared.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.persistence.annotations.UuidGenerator;
import pl.bank.shared.util.converter.UUIDAttributeConverter;
import pl.bank.shared.util.converter.UUIDDeserializer;

import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@EqualsAndHashCode(callSuper = true, exclude = {"user", "transfers"})
@ToString(exclude = {"user", "transfers"})
@Table(name = "Accounts")
@NamedQueries({
        @NamedQuery(
                name = "findUserAccounts",
                query = "SELECT a FROM Account a WHERE a.user.id = :id"
        )
})
//@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "account", propOrder = {
        "iban",
        "user"
})
public class Account extends BaseEntity<UUID> {
    private static final long serialVersionUID = 1L;
    public static final String FIND_USER_ACCOUNTS = "findUserAccounts";

    @Column(columnDefinition = "uuid")
    @Id
    @Convert(converter = UUIDAttributeConverter.class)
    @UuidGenerator(name = "uuid")
    @XmlTransient
    @JsonbTypeDeserializer(UUIDDeserializer.class)
    private UUID id;

    @Column
    private String iban;

    @XmlTransient
    @Column
    private String name;

    @XmlTransient
    @Column
    private Double balance;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    @XmlTransient
    @JsonIgnore
    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Set<Transfer> transfers = new HashSet<>();
}
