package pl.bank.shared.dao;

import pl.bank.shared.domain.model.BaseEntity;

import javax.ejb.*;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.ws.rs.NotFoundException;
import java.io.Serializable;
import java.util.*;

@Startup
@Singleton
public class GenericDao {

    /*
     * A "copy" of the Hibernate's API as this doesn't exist
     * in JPA.
     */
    public enum MatchMode { START, END, EXACT, ANYWHERE }

    @Inject
    protected EntityManager entityManager;

    /**
     * Saves an entity.
     *
     * @param entity
     * @return newly created id for the entity.
     */
    public <T extends BaseEntity<PK>, PK extends Serializable> PK save(T entity) {
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.persist(entity);
        entityManager.flush();
        entityManager.clear();
        entityManager.getTransaction().commit();
        return entity.getId();
    }


    /**
     * Marges objects with the same identifier within a session into a newly
     * created object.
     *
     * @param entity
     * @return a newly created instance merged.
     */
    public <T extends BaseEntity<PK>, PK extends Serializable> T merge(T entity) {
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        T newEntity = entityManager.merge(entity);
        entityManager.flush();
        entityManager.clear();
        entityManager.getTransaction().commit();
        return newEntity;
    }

    /**
     * Deletes tne entity.
     *
     * @param clazz
     * @param id
     * @throws NotFoundException if the id does not exist.
     */
    public <T extends BaseEntity<PK>, PK extends Serializable> void delete(Class<T> clazz, PK id) throws NotFoundException {
        Optional<T> entity = find(clazz, id);
        if (entity.isPresent()) {
            if (!entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().begin();
            }
            entityManager.remove(entity.get());
            entityManager.flush();
            entityManager.clear();
            entityManager.getTransaction().commit();
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Find an entity by its identifier.
     *
     * @param clazz
     * @param id
     * @return
     */
    public <T extends BaseEntity<?>> Optional<T> find(Class<T> clazz, Serializable id) {
        return Optional.ofNullable(entityManager.find(clazz, id));
    }

    /**
     * Finds an entity by one of its properties.
     *
     *
     * @param clazz the entity class.
     * @param propertyName the property name.
     * @param value the value by which to find.
     * @return
     */
    public <T extends BaseEntity<?>> Set<T> findByProperty(Class<T> clazz, String propertyName, Object value) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(clazz);
        Root<T> root = cq.from(clazz);
        cq.where(cb.equal(root.get(propertyName), value));
        return new HashSet<>(entityManager.createQuery(cq).getResultList());
    }

    /**
     * Finds entities by a String property specifying a MatchMode. This search
     * is case insensitive.
     *
     * @param clazz the entity class.
     * @param propertyName the property name.
     * @param value the value to check against.
     * @param matchMode the match mode: EXACT, START, END, ANYWHERE.
     * @return
     */
    public <T extends BaseEntity<?>> Set<T> findByProperty(Class<T> clazz, String propertyName, String value, MatchMode matchMode) {
        //convert the value String to lowercase
        value = value.toLowerCase();
        if (MatchMode.START.equals(matchMode)) {
            value = value + "%";
        } else if (MatchMode.END.equals(matchMode)) {
            value = "%" + value;
        } else if (MatchMode.ANYWHERE.equals(matchMode)) {
            value = "%" + value + "%";
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(clazz);
        Root<T> root = cq.from(clazz);
        cq.where(cb.like(cb.lower(root.get(propertyName)), value));

        return new HashSet<>(entityManager.createQuery(cq).getResultList());
    }



    /**
     * Finds all objects of an entity class.
     *
     * @param clazz the entity class.
     * @return
     */
    public <T extends BaseEntity<?>> Set<T> findAll(Class<T> clazz) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(clazz);
        cq.from(clazz);
        return new HashSet<>(entityManager.createQuery(cq).getResultList());
    }

    /**
     * Finds all objects of a class by the specified order.
     *
     * @param clazz the entity class.
     * @param order the order: ASC or DESC.
     * @param propertiesOrder the properties on which to apply the ordering.
     *
     * @return
     */
    public <T extends BaseEntity<?>> Set<T> findAll(Class<T> clazz, QueryOrder order, String... propertiesOrder) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(clazz);
        Root<T> root = cq.from(clazz);

        List<Order> orders = new ArrayList<>();
        for (String propertyOrder : propertiesOrder) {
            if (order.isAscOrder()) {
                orders.add(cb.asc(root.get(propertyOrder)));
            } else {
                orders.add(cb.desc(root.get(propertyOrder)));
            }
        }
        cq.orderBy(orders);

        return new HashSet<>(entityManager.createQuery(cq).getResultList());
    }

    /**
     * @author Krzysztof Adamczyk
     */
    public <T extends BaseEntity<?>> Set<T> executeNamedQuery(String queryName, Map<String, Object> queryParameters) {
        Query namedQuery = entityManager.createNamedQuery(queryName);
        queryParameters.forEach(namedQuery::setParameter);
        return new HashSet<T>(namedQuery.getResultList());
    }

}
