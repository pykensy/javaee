package pl.bank.shared.generators.mock;

import pl.bank.shared.domain.model.User;
import pl.bank.shared.generators.UsernameGenerator;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static pl.bank.shared.generators.mock.MockData.FIRST_NAMES;
import static pl.bank.shared.generators.mock.MockData.LAST_NAMES;

public class UsersGenerator {

    public static final String DEFAULT_USER_PASSWORD = "WZRHGrsBESr8wYFZ9sx0tPURuZgG2lmzyvWpwXPKz8U=";

    private static final Random random = new Random();

    public static User generateUser() {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setPassword(DEFAULT_USER_PASSWORD);
        user.setFirstName(FIRST_NAMES[Math.abs(random.nextInt()) % FIRST_NAMES.length]);
        user.setLastName(LAST_NAMES[Math.abs(random.nextInt()) % LAST_NAMES.length]);
        user.setRole("USER");
        user.setUsername(UsernameGenerator.generate());

        return user;
    }

    public static Set<User> generateUsers(int numberOfUsers) {
        Set<User> users = new HashSet<>();
        for (int i = 0; i < numberOfUsers; i++) {
            users.add(generateUser());
        }
        return users;
    }
}
