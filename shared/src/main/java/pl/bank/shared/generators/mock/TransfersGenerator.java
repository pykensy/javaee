package pl.bank.shared.generators.mock;

import pl.bank.shared.domain.model.Beneficiary;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.transfer.TransferRequest;
import pl.bank.shared.utils.TokenGenerator;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static pl.bank.shared.generators.mock.MockData.*;

public class TransfersGenerator {

    private static int minDay = (int) LocalDate.of(2016, 1, 1).toEpochDay();
    private static int maxDay = (int) LocalDate.of(2018, 12, 30).toEpochDay();

    private static final Random random = new Random();

    public static Transfer generateTransfer(Transfer.TransferDirect direct) {
        Transfer transfer = new Transfer();
        transfer.setId(UUID.randomUUID());

        long randomDay = minDay + random.nextInt(maxDay - minDay);
        LocalDate randomDate = LocalDate.ofEpochDay(randomDay);
        transfer.setTransactionDate(randomDate);

        transfer.setTitle(TITLES[Math.abs(random.nextInt()) % TITLES.length]);

        double amount = MIN_BALANCE + (random.nextDouble() * (MAX_BALANCE - MIN_BALANCE));

        transfer.setAmount(Double.valueOf(df.format(amount).replace(",", ".")));

        transfer.setStatus(Transfer.TransferStatuses.EXECUTED.getValue());
        transfer.setDirect(direct.getValue());
        return transfer;
    }

    public static Set<Transfer> generateTransfers(int numberOfTransfers) {
        Set<Transfer> transfers = new HashSet<>();

        return transfers;
    }

    public static TransferRequest generateTransferRequest(String userId, String accountId, String typeId) {
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setUserId(userId);

        transferRequest.setAccountId(accountId);

        Beneficiary beneficiary = BeneficiariesGenerator.generateBeneficiary();
        transferRequest.setBeneficiaryName(beneficiary.getFullName());
        transferRequest.setBeneficiaryAccountIban(beneficiary.getAccountIban());
        transferRequest.setBeneficiaryAddress(beneficiary.getAddress());

        transferRequest.setTransferTypeId(typeId);

        double amount = MIN_BALANCE + (random.nextDouble() * (MAX_BALANCE - MIN_BALANCE));
        transferRequest.setAmount(Double.valueOf(df.format(amount).replace(",", ".")));

        transferRequest.setTitle(TITLES[Math.abs(random.nextInt()) % TITLES.length]);
        int minDay = (int) LocalDate.now().toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);
        LocalDate randomDate = LocalDate.ofEpochDay(randomDay);

        String day = Integer.toString(randomDate.getDayOfMonth());
        if (randomDate.getDayOfMonth() < 10) {
            day = "0" + day;
        }
        String month = Integer.toString(randomDate.getMonthValue());
        if (randomDate.getMonthValue() < 10) {
            month = "0" + month;
        }

        String date = day + "." + month + "." + Integer.toString(randomDate.getYear());
        transferRequest.setDate(date);

        transferRequest.setEmailConfirm(random.nextBoolean());

        if (transferRequest.isEmailConfirm()) {
            transferRequest.setEmail("wipekxxx@gmail.com");
        }

        transferRequest.setToken(TokenGenerator.generateToken());

        return  transferRequest;
    }
}
