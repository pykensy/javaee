package pl.bank.shared.generators.mock;

import pl.bank.shared.domain.model.TransferType;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static pl.bank.shared.generators.mock.MockData.TRANSFER_TYPES;

public class TransferTypesGenerator {

    public static Set<TransferType> generateTransferTypes() {
        Set<TransferType> transferTypes = new HashSet<>();

        Double transferCost = 5.0;
        for (int i = 0; i < TRANSFER_TYPES.length; i++) {

            TransferType transferType = new TransferType();
            transferType.setId(UUID.randomUUID());
            transferType.setName(TRANSFER_TYPES[i]);
            transferType.setPrice(transferCost);
            transferTypes.add(transferType);
            transferCost+=5;
        }

        return transferTypes;
    }
}
