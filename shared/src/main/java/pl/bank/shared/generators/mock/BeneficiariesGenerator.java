package pl.bank.shared.generators.mock;

import pl.bank.shared.domain.model.Beneficiary;
import pl.bank.shared.generators.IbanGenerator;

import java.util.Random;
import java.util.UUID;

import static pl.bank.shared.generators.mock.MockData.ADDRESSES;
import static pl.bank.shared.generators.mock.MockData.FIRST_NAMES;
import static pl.bank.shared.generators.mock.MockData.LAST_NAMES;

public class BeneficiariesGenerator {

    private static final Random random = new Random();

    public static Beneficiary generateBeneficiary() {
        Beneficiary beneficiary = new Beneficiary();
        beneficiary.setId(UUID.randomUUID());

        String firstName = FIRST_NAMES[Math.abs(random.nextInt()) % FIRST_NAMES.length];
        String lastName = LAST_NAMES[Math.abs(random.nextInt()) % LAST_NAMES.length];
        beneficiary.setFullName(firstName + " " + lastName);
        beneficiary.setAccountIban(IbanGenerator.generate());
        beneficiary.setAddress(ADDRESSES[Math.abs(random.nextInt()) % ADDRESSES.length]);

        return beneficiary;
    }
}
