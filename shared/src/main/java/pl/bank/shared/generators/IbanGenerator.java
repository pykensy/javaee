package pl.bank.shared.generators;

import java.util.Random;

public class IbanGenerator {

    private static int IBAN_LENGTH = 28;

    public static String generate() {
        StringBuilder iban = new StringBuilder();
        iban.append("PL");

        Random random = new Random();

        for (int i = 2; i < IBAN_LENGTH; i++) {
            int number = random.nextInt(10);
            iban.append(Integer.toString(number));
        }

        return iban.toString();
    }
}
