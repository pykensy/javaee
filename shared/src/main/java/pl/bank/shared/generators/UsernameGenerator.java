package pl.bank.shared.generators;

import java.util.Random;

public class UsernameGenerator {

    public static final int USERNAME_LENGTH = 12;

    public static String generate() {
        StringBuilder username = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < USERNAME_LENGTH; i++) {
            username.append(Integer.toString(Math.abs(random.nextInt(9))));
        }
        return username.toString();
    }
}
