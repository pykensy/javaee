package pl.bank.shared.generators.mock;

import pl.bank.shared.domain.model.Account;
import pl.bank.shared.generators.IbanGenerator;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static pl.bank.shared.generators.mock.MockData.*;

public class AccountsGenerator {

    private static final Random random = new Random();

    public static Account generateAccount() {
        Account account = new Account();
        account.setId(UUID.randomUUID());

        double balance = MIN_BALANCE + (random.nextDouble() * (MAX_BALANCE - MIN_BALANCE));
        account.setBalance(Double.valueOf(df.format(balance).replace(",", ".")));

        account.setIban(IbanGenerator.generate());
        account.setName(ACCOUNT_NAMES[(int)(System.currentTimeMillis() % ACCOUNT_NAMES.length)]);

        return account;
    }

    public static Set<Account> generateAccounts(int numberOfAccounts) {
        Set<Account> accounts = new HashSet<>();

        for (int i = 0; i < numberOfAccounts; i++) {
            accounts.add(generateAccount());
        }

        return accounts;
    }
}
