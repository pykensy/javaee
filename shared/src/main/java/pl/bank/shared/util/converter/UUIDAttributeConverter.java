package pl.bank.shared.util.converter;

import javax.persistence.AttributeConverter;
import java.util.UUID;

@javax.persistence.Converter(autoApply = true)
public class UUIDAttributeConverter implements AttributeConverter<UUID, UUID> {
    @Override
    public UUID convertToDatabaseColumn(UUID uuid) {
        return uuid;
    }

    @Override
    public UUID convertToEntityAttribute(UUID uuid) {
        return uuid;
    }
}