package pl.bank.shared.util.converter;

import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.stream.JsonParser;
import java.lang.reflect.Type;
import java.util.UUID;

public class UUIDDeserializer implements JsonbDeserializer<UUID> {
    @Override
    public UUID deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
        return UUID.fromString(parser.getString());
    }
}
