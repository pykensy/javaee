package pl.bank.emails.transfer.token.control;

import org.slf4j.Logger;
import pl.bank.emails.sender.EmailSender;
import pl.bank.emails.transfer.token.entity.TokenNotSendYet;
import pl.bank.transfers.events.transfer.entity.command.SendTransferConfirmationEmailCommand;

import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.mail.MessagingException;
import java.io.IOException;

@Stateless
public class TokenEmailService {

    @Inject
    private Logger logger;

    private final static String MOCK_EMAIL = "wipekxxx@gmail.com";

    public void onTokenSendRequest(final @Observes TokenNotSendYet tokenNotSendYet) {
        String subject = "Token przelewowy";
        String mailTo = MOCK_EMAIL;
        StringBuilder message = new StringBuilder();
        message.append(tokenNotSendYet.getTransferRequestForToken().getToken());

        try {
            EmailSender.sendEmail(subject, message.toString(), mailTo);
            logger.info("Email send");
        } catch (IOException | MessagingException e) {
            e.printStackTrace();
            logger.error(e.toString());
        }
    }

    public void onEmailConfirmationEmailSend(final @Observes SendTransferConfirmationEmailCommand command) {
        String subject = "Potwierdzenie przelewu";
        String mailTo = command.getEmail();
        StringBuilder message = new StringBuilder();
        message.append("Z konta ").append(command.getTransfer().getAccount().getIban()).append("\n");
        message.append("Na konto ").append(command.getTransfer().getBeneficiary().getAccountIban()).append("\n");
        message.append("Odbiorca ").append(command.getTransfer().getBeneficiary().getFullName()).append("\n");

        String address = command.getTransfer().getBeneficiary().getAddress();

        message.append("Adres odbiorcy ").append(address != null ? address : "").append("\n");
        message.append("Kwota ").append(command.getTransfer().getAmount()).append("\n");
        message.append("Przewidywana data wykonania ").append(command.getTransfer().getTransactionDate()).append("\n");
        message.append("Typ przelewu ").append(command.getTransfer().getType().getName()).append("\n");
        message.append("Tytuł ").append(command.getTransfer().getTitle()).append("\n");

        try {
            EmailSender.sendEmail(subject, message.toString(), mailTo);
            logger.info("Email send");
        } catch (IOException | MessagingException e) {
            e.printStackTrace();
            logger.error(e.toString());
        }
    }
}
