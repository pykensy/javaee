package pl.bank.emails.transfer.token.jms;

import org.slf4j.Logger;
import pl.bank.config.resource.Resources;
import pl.bank.emails.transfer.token.entity.TokenNotSendYet;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

//@Stateless
//@Singleton
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup",
                propertyValue = Resources.EMAIL_QUEUE),
        @ActivationConfigProperty(propertyName = "destinationType",
                propertyValue = "javax.jms.Queue"),})
public class EmailMessageReceiver implements MessageListener {

    @Inject
    private Event<TokenNotSendYet> jmsEvent;

    @Inject
    private Logger logger;

    @Override
    public void onMessage(Message message) {
//        try {
        // todo
            logger.info(message.toString());
//            TransferRequestForToken transferRequestForToken = message.getBody(TransferRequestForToken.class);

//            jmsEvent.fire(new TokenNotSendYet(transferRequestForToken));
//        } catch (JMSException e) {
//            e.printStackTrace();
//        }
    }
}
