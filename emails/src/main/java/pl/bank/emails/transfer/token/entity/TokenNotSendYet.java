package pl.bank.emails.transfer.token.entity;

import pl.bank.transfers.events.token.entity.TokenEvent;
import pl.bank.transfers.events.token.entity.TransferRequestForToken;

public class TokenNotSendYet extends TokenEvent {
    private static final long serialVersionUID = 1L;
    public TokenNotSendYet(TransferRequestForToken transferRequestForToken) {
        super(transferRequestForToken);
    }
}
