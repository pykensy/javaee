package pl.bank.transfers.events.transfer.entity.command;

import lombok.Getter;
import pl.bank.shared.domain.model.Transfer;

public class SaveTransferCommand implements TransferCommand {
    private static final long serialVersionUID = 1L;

    @Getter
    private Transfer transfer;

    public SaveTransferCommand(Transfer transfer) {
        this.transfer = transfer;
    }
}
