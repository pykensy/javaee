package pl.bank.transfers.events.transfer.entity.command;

import lombok.Getter;
import pl.bank.shared.domain.model.Transfer;

public class SendTransferConfirmationEmailCommand implements TransferCommand {
    private static final long serialVersionUID = 1L;

    @Getter
    private Transfer transfer;

    @Getter
    private String email;

    public SendTransferConfirmationEmailCommand(Transfer transfer, String email) {
        this.transfer = transfer;
        this.email = email;
    }
}
