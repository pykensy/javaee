package pl.bank.transfers.events.token.entity;

import lombok.Getter;

import java.io.Serializable;

public abstract class TokenEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    @Getter
    private TransferRequestForToken transferRequestForToken;

    public TokenEvent() {
    }

    public TokenEvent(TransferRequestForToken transferRequestForToken) {
        this.transferRequestForToken = transferRequestForToken;
    }
}
