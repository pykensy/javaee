package pl.bank.transfers.events.transfer.entity.command;

import lombok.Getter;
import pl.bank.shared.domain.model.transfer.TransferRequest;

@Getter
public class PrepareTransferCommand implements TransferCommand {
    private static final long serialVersionUID = 1L;

    private TransferRequest transferRequest;

    public PrepareTransferCommand(TransferRequest transferRequest) {
        this.transferRequest = transferRequest;
    }
}
