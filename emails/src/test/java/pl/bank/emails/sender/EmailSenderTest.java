package pl.bank.emails.sender;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.mail.MessagingException;
import java.io.IOException;

@RunWith(Arquillian.class)
public class EmailSenderTest {
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(EmailSender.class)
                .addAsResource("email.properties")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void sendEmail() throws IOException, MessagingException {
        EmailSender.sendEmail("Test", "Testowy email", "wipekxxx@gmail.com");
    }
}
