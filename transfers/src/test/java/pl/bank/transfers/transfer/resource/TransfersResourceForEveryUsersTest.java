package pl.bank.transfers.transfer.resource;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bank.config.MockResource;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Account;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.shared.domain.model.User;
import pl.bank.shared.domain.model.transfer.TransferRequest;
import pl.bank.shared.generators.mock.TransfersGenerator;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;
import static pl.bank.shared.generators.mock.MockData.DEFAULT_USER_PASSWORD;

@RunWith(Arquillian.class)
public class TransfersResourceForEveryUsersTest {
    private static WebTarget transferClient;
    private static WebTarget loginClient;

    @ArquillianResource
    private URL base;

    private String token;

    private User user;

    private EntityManager entityManager;

    private Set<User> users = new HashSet<>();

    private GenericDao dao;

    private Set<TransferType> transferTypes;

    private Set<Account> accounts;

    private Logger logger = LoggerFactory.getLogger(TransfersResourceForEveryUsersTest.class);

    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, "pl.bank")
                .addAsResource("META-INF/beans.xml");
    }

    @Before
    public void setUp() throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("h2");
        entityManager = entityManagerFactory.createEntityManager();

        MockResource mockResource = MockResource.getInstance();

        loginClient = ClientBuilder.newClient().target(new URL(base, "api/auth/token").toExternalForm());

        dao = new GenericDao();
        Field daoEmField = GenericDao.class.getDeclaredField("entityManager");
        daoEmField.setAccessible(true);
        daoEmField.set(dao, entityManager);

        users = dao.findAll(User.class);
        transferTypes = dao.findAll(TransferType.class);

        accounts = dao.findAll(Account.class);

//        users.forEach(user -> {
//            Set<Account> userAccounts = new HashSet<>(entityManager.createQuery("SELECT a FROM Account a WHERE a.user.id = :userId")
//                    .setParameter("userId", user.getId()).getResultList());
//            user.setAccounts(userAccounts);
//        });
    }

    @InSequence(1)
    @RunAsClient
    @Test
    public void tryExecute() throws MalformedURLException {
        transferClient = ClientBuilder
                .newClient()
                .target(new URL(base, "api/transfers").toExternalForm());
        long numberOfTransfersBeforeSave = (long) entityManager.createQuery("SELECT COUNT (t) FROM Transfer t").getSingleResult();

        long start = System.currentTimeMillis();
        accounts.parallelStream().forEach(account -> {
            user = account.getUser();
            String token = login(user.getUsername(), DEFAULT_USER_PASSWORD);
            String authHeader = "Bearer " + token;

            String transferTypeId = transferTypes.stream().findAny().get().getId().toString();

            TransferRequest transferRequest = TransfersGenerator.generateTransferRequest(user.getId().toString(), account.getId().toString(), transferTypeId);

            transferClient
                    .request()
                    .header("Authorization", authHeader)
                    .post(Entity.json(transferRequest));
        });
        long stop = System.currentTimeMillis();
        logger.info(Long.toString(stop - start));
        long numberOfTransfersAfterSave = (long) entityManager.createQuery("SELECT COUNT (t) FROM Transfer t").getSingleResult();
        Assert.assertEquals(numberOfTransfersBeforeSave + accounts.size(), numberOfTransfersAfterSave);
    }

    private String login(String username, String password) {
        Form form = new Form();
        form
                .param("username", username)
                .param("password", password);

        Response authResponse = loginClient
                .request(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.form(form));
        return authResponse.readEntity(String.class);
    }
}
