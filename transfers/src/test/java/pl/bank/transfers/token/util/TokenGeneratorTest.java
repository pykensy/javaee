package pl.bank.transfers.token.util;

import org.junit.Test;
import pl.bank.shared.utils.TokenGenerator;

import static org.junit.Assert.*;
import static pl.bank.shared.utils.TokenGenerator.TOKEN_LENGTH;

public class TokenGeneratorTest {

    @Test
    public void generateToken() {
        assertEquals(TokenGenerator.generateToken().length(), TOKEN_LENGTH);
    }
}