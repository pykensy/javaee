package pl.bank.transfers.token.query;

import pl.bank.transfers.token.store.TokenStoreService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.Serializable;

@Stateless
public class TokenQueryService implements Serializable {

    @EJB
    private TokenStoreService tokenStoreService;

    public String getToken() {
        return tokenStoreService.getTransferRequestForToken().getToken();
    }
}
