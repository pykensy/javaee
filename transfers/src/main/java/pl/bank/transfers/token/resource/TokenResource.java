package pl.bank.transfers.token.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import pl.bank.security.filters.JWTTokenNeeded;
import pl.bank.shared.domain.model.transfer.TransferRequest;
import pl.bank.transfers.events.token.entity.TransferRequestForToken;
import pl.bank.transfers.token.command.TokenCommandService;
import pl.bank.transfers.token.query.TokenQueryService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static pl.bank.config.api.ApplicationConfig.*;

@Path("transfers")
@Stateless
public class TokenResource {

    @Inject
    private Logger logger;

    @EJB
    private TokenCommandService tokenCommandService;

    @Inject
    private TokenQueryService tokenQueryService;

    @JWTTokenNeeded(roles = {ADMIN_ROLE, EMPLOYEE_ROLE, USER_ROLE})
    @POST
    @Path("/token")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateToken(TransferRequest transferRequest) {
        logger.info(transferRequest.toString());
        tokenCommandService.generateToken(new TransferRequestForToken(transferRequest));

        ObjectMapper mapper = new ObjectMapper();
        String resultJson = null;
        try {
            resultJson = mapper.writeValueAsString(tokenQueryService.getToken());
        } catch (JsonProcessingException e) {
            logger.error(e.toString());
            e.printStackTrace();
        }

        return Response.ok().entity(resultJson).build();
    }

}
