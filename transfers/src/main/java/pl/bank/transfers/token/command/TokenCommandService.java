package pl.bank.transfers.token.command;

import org.slf4j.Logger;
import pl.bank.transfers.events.token.entity.TokenGenerated;
import pl.bank.transfers.events.token.entity.TransferRequestForToken;
import pl.bank.transfers.token.store.TokenStoreService;
import pl.bank.shared.utils.TokenGenerator;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

@Stateless
public class TokenCommandService {

    @Inject
    private Logger logger;

    @Inject
    private Event<TokenGenerated> jmsEvent;

    @EJB
    private TokenStoreService tokenStoreService;

    public void generateToken(TransferRequestForToken transferRequestForToken) {
        generate(transferRequestForToken);
        jmsEvent.fire(new TokenGenerated(transferRequestForToken));
        tokenStoreService.saveTransferRequestForTokenInStore(transferRequestForToken);
    }

    private void generate(TransferRequestForToken transferRequestForToken) {
        String token = TokenGenerator.generateToken();
        logger.info("Token generated: " + token + "for request: " + transferRequestForToken.getTransferRequest());
        transferRequestForToken.setToken(token);
    }
}
