package pl.bank.transfers.token.store;

import lombok.Getter;
import lombok.Setter;
import pl.bank.transfers.events.token.entity.TransferRequestForToken;

import javax.ejb.Stateless;
import java.io.Serializable;

@Stateless
public class TokenStoreService implements Serializable {

    @Setter
    @Getter
    private TransferRequestForToken transferRequestForToken;

    public void saveTransferRequestForTokenInStore(TransferRequestForToken transferRequestForToken) {
        this.transferRequestForToken = transferRequestForToken;
    }
}
