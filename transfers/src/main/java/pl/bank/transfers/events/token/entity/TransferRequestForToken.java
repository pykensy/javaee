package pl.bank.transfers.events.token.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.bank.shared.domain.model.transfer.TransferRequest;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@ToString
@EqualsAndHashCode
@Getter
public class TransferRequestForToken implements Serializable {
    private static final long serialVersionUID = 1L;

    private final UUID id;
    private final Instant instant;
    private final TransferRequest transferRequest;

    @Setter
    private String token;

    public TransferRequestForToken(TransferRequest transferRequest) {
        id = UUID.randomUUID();
        instant = Instant.now();
        this.transferRequest = transferRequest;
    }
}
