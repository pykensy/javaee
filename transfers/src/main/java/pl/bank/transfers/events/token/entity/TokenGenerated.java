package pl.bank.transfers.events.token.entity;

public class TokenGenerated extends TokenEvent {
    private static final long serialVersionUID = 1L;
    public TokenGenerated(TransferRequestForToken transferRequestForToken) {
        super(transferRequestForToken);
    }
}
