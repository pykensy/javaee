package pl.bank.transfers.transfer.resource;

import org.slf4j.Logger;
import pl.bank.security.filters.JWTTokenNeeded;
import pl.bank.shared.domain.model.transfer.TransferRequest;
import pl.bank.transfers.events.transfer.entity.command.PrepareTransferCommand;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static pl.bank.config.api.ApplicationConfig.*;

@Path("transfers")
@Stateless
public class TransfersResource {

    @Inject
    private Logger logger;

    @Inject
    private Event<PrepareTransferCommand> jmsEvent;

    @JWTTokenNeeded(roles = {ADMIN_ROLE, EMPLOYEE_ROLE, USER_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response tryExecute(TransferRequest transferRequest) {
        logger.info(transferRequest.toString());

        jmsEvent.fire(new PrepareTransferCommand(transferRequest));

        return Response.ok().status(200).build();
    }


}
