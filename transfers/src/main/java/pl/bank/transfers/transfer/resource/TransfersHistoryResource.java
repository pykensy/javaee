package pl.bank.transfers.transfer.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.bank.security.filters.JWTTokenNeeded;
import pl.bank.transfers.transfer.query.TransferQueryService;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

import static pl.bank.config.api.ApplicationConfig.*;

@Path("transfers")
public class TransfersHistoryResource {

    @EJB
    private TransferQueryService queryService;

    @JWTTokenNeeded(roles = {ADMIN_ROLE, EMPLOYEE_ROLE, USER_ROLE})
    @GET
    @Path("/account/history/outgoing/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_PLAIN)
    public Response getOutgoingAccountsTransfersHistory(@PathParam("id") UUID id) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String resultJson = mapper.writeValueAsString(queryService.getOutgoingAccountsTransfersHistory(id));
        return Response.ok().entity(resultJson).build();
    }

    @JWTTokenNeeded(roles = {ADMIN_ROLE, EMPLOYEE_ROLE, USER_ROLE})
    @GET
    @Path("/account/history/incoming/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_PLAIN)
    public Response getIncomingAccountsTransfersHistory(@PathParam("id") UUID id) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String resultJson = mapper.writeValueAsString(queryService.getIncomingAccountsTransfersHistory(id));
        return Response.ok().entity(resultJson).build();
    }
}
