package pl.bank.transfers.transfer.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.bank.security.filters.JWTTokenNeeded;
import pl.bank.transfers.transfer.query.TransferQueryService;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static pl.bank.config.api.ApplicationConfig.*;

@Path("transfer")
public class TransferTypesResource {

    @EJB
    private TransferQueryService queryService;

    @JWTTokenNeeded(roles = {ADMIN_ROLE, EMPLOYEE_ROLE, USER_ROLE})
    @GET
    @Path("/types")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransferTypes() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String resultJson = mapper.writeValueAsString(queryService.getTransferTypes());
        return Response.ok().entity(resultJson).build();
    }
}
