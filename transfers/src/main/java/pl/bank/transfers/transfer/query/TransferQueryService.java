package pl.bank.transfers.transfer.query;

import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Account;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static pl.bank.shared.domain.model.Transfer.GET_ACCOUNTS_TRANSFERS_HISTORY;

@Stateless
public class TransferQueryService {

    @EJB
    private GenericDao dao;

    public Set<Transfer> getOutgoingAccountsTransfersHistory(UUID id) {
        Map<String, Object> queryParameters = new HashMap<>(0);
        queryParameters.put("id", id);
        queryParameters.put("direct", Transfer.TransferDirect.OUTGOING.getValue());
        return dao.executeNamedQuery(GET_ACCOUNTS_TRANSFERS_HISTORY, queryParameters);
    }

    public Set<Transfer> getIncomingAccountsTransfersHistory(UUID id) {
        Map<String, Object> queryParameters = new HashMap<>(0);
        queryParameters.put("id", id);
        queryParameters.put("direct", Transfer.TransferDirect.INCOMING.getValue());
        return dao.executeNamedQuery(GET_ACCOUNTS_TRANSFERS_HISTORY, queryParameters);
    }

    public Set<TransferType> getTransferTypes() {
        return dao.findAll(TransferType.class);
    }

    public boolean isBeneficiaryInternal(String accountIban) {
        return dao.findByProperty(Account.class, "iban", accountIban).size() == 1;
    }
}
