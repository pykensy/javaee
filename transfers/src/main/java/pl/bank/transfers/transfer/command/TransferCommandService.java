package pl.bank.transfers.transfer.command;

import org.slf4j.Logger;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Account;
import pl.bank.shared.domain.model.Beneficiary;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.shared.domain.model.transfer.TransferRequest;
import pl.bank.transfers.events.transfer.entity.command.PrepareTransferCommand;
import pl.bank.transfers.events.transfer.entity.command.SaveTransferCommand;
import pl.bank.transfers.events.transfer.entity.command.SendTransferConfirmationEmailCommand;
import pl.bank.transfers.events.transfer.entity.command.TransferCommand;
import pl.bank.transfers.transfer.query.TransferQueryService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static pl.bank.config.api.ApplicationConfig.TRANSFER_DATE_FORMAT;

@Stateless
public class TransferCommandService {

    @Inject
    private Logger logger;

    @EJB
    private GenericDao dao;

    @Inject
    private Event<TransferCommand> jmsEvent;

    @EJB
    private TransferQueryService queryService;

    public void onPrepareTransferCommand(final @Observes PrepareTransferCommand prepareTransferCommand) throws ParseException {
        TransferRequest transferRequest = prepareTransferCommand.getTransferRequest();

        Transfer transfer = convertToTransferFromRequest(transferRequest);

        jmsEvent.fire(new SaveTransferCommand(transfer));
        if (transferRequest.isEmailConfirm()) {
            jmsEvent.fire(new SendTransferConfirmationEmailCommand(transfer, transferRequest.getEmail()));
        }
    }

    public void onSaveTransferCommand(final @Observes SaveTransferCommand saveTransferCommand) {
        Transfer transfer = saveTransferCommand.getTransfer();
        Double oldAccountBalance = transfer.getAccount().getBalance();
        Double newAccountBalance = oldAccountBalance - transfer.getAmount() - transfer.getType().getPrice();
        transfer.getAccount().setBalance(newAccountBalance);
        transfer.setStatus(Transfer.TransferStatuses.SAVED.getValue());
        transfer.setId(UUID.randomUUID());
        dao.merge(transfer);
    }

    private Account getSourceAccount(String id) {
        return dao.find(Account.class, UUID.fromString(id)).orElseThrow(EntityNotFoundException::new);
    }

    private Beneficiary getBeneficiary(TransferRequest transferRequest) {
        Beneficiary beneficiary = new Beneficiary();
        beneficiary.setId(UUID.randomUUID());
        beneficiary.setAccountIban(transferRequest.getBeneficiaryAccountIban());
        beneficiary.setAddress(transferRequest.getBeneficiaryAddress());
        beneficiary.setFullName(transferRequest.getBeneficiaryName());

        return beneficiary;
    }

    private TransferType getTransferType(String id) {
        return dao.find(TransferType.class, UUID.fromString(id)).orElseThrow(EntityNotFoundException::new);
    }

    public Transfer convertToTransferFromRequest(TransferRequest transferRequest) throws ParseException {
        Transfer transfer = new Transfer();

        transfer.setAccount(getSourceAccount(transferRequest.getAccountId()));
        transfer.setAmount(transferRequest.getAmount());

        transfer.setBeneficiary(getBeneficiary(transferRequest));
        transfer.setTitle(transferRequest.getTitle());

        boolean isInternalBeneficiary = queryService.isBeneficiaryInternal(transfer.getBeneficiary().getAccountIban());

        if (isInternalBeneficiary) {
            transfer.setType(dao.findByProperty(TransferType.class, "name", TransferType.ELIXIR_NAME).stream().findFirst().get());
        } else {
            transfer.setType(getTransferType(transferRequest.getTransferTypeId()));
        }

        LocalDate date = isDateRequired(transfer.getType().getName()) ? LocalDate.parse(transferRequest.getDate(), DateTimeFormatter.ofPattern(TRANSFER_DATE_FORMAT)) : LocalDate.now();
        transfer.setTransactionDate(date);
        logger.info(transfer.toString());

        return transfer;
    }

    private boolean isDateRequired(String type) {
        return type.equals(TransferType.Types.ELIXIR.getValue());
    }
}
