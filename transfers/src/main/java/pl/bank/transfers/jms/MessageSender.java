package pl.bank.transfers.jms;

import org.slf4j.Logger;
import pl.bank.config.resource.Resources;
import pl.bank.transfers.events.token.entity.TokenGenerated;
import pl.bank.transfers.events.transfer.entity.command.SendTransferConfirmationEmailCommand;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.*;
import java.io.Serializable;

@Stateless
@Singleton
public class MessageSender {

    @Resource(lookup = "java:comp/DefaultJMSConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(lookup = Resources.EMAIL_QUEUE)
    private Queue queue;

    @Inject
    private Logger logger;

    public void sendMessage(Serializable request) throws JMSException {
        try (Connection connection = connectionFactory.createConnection()) {
            connection.start();
            Session session = connection.createSession(Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(queue);
            ObjectMessage objectMessage = session.createObjectMessage(request);
            messageProducer.send(objectMessage);
        }
    }

    public void onTokenGenerated(final @Observes TokenGenerated tokenGenerated) {
        try {
            sendMessage(tokenGenerated.getTransferRequestForToken());
        } catch (JMSException e) {
            e.printStackTrace();
            logger.error(e.toString());
        }
    }

    public void onConfirmationTransferEmailSend(final @Observes SendTransferConfirmationEmailCommand command) {
        try {
            sendMessage(command);
        } catch (JMSException e) {
            e.printStackTrace();
            logger.error(e.toString());
        }
    }
}
