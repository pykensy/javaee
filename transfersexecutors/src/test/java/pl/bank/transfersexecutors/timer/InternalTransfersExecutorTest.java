package pl.bank.transfersexecutors.timer;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bank.config.MockResource;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.transfersexecutors.command.TransferExecutorsCommandService;
import pl.bank.transfersexecutors.query.TransferExecutorsQueryService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.soap.SOAPException;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

//@RunWith(Arquillian.class)
public class InternalTransfersExecutorTest {

    private EntityManager entityManager;

    private InternalTransfersExecutor internalTransfersExecutor;

    private TransferExecutorsQueryService queryService;
    private TransferExecutorsCommandService commandService;
    private Logger logger;
    private MockResource mockResource;

//    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(InternalTransfersExecutor.class)
                .addPackages(true, "pl.bank")
                .addAsResource("META-INF/persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

//    @Before
    public void setUp() throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("h2");
        entityManager = entityManagerFactory.createEntityManager();
        MockResource mockResource = MockResource.getInstance();

        GenericDao genericDao = new GenericDao();

        Field emDaoField = GenericDao.class.getDeclaredField("entityManager");
        emDaoField.setAccessible(true);
        emDaoField.set(genericDao, entityManager);

        logger = LoggerFactory.getLogger(TransferExecutor.class);

        queryService = new TransferExecutorsQueryService();
        commandService = new TransferExecutorsCommandService();

        Field queryServiceDaoField = TransferExecutorsQueryService.class.getDeclaredField("dao");
        queryServiceDaoField.setAccessible(true);
        queryServiceDaoField.set(queryService, genericDao);

        Field commandServiceDaoField = TransferExecutorsCommandService.class.getDeclaredField("dao");
        commandServiceDaoField.setAccessible(true);
        commandServiceDaoField.set(commandService, genericDao);


        internalTransfersExecutor = new InternalTransfersExecutor();

        Field internalTransfersExecutorQueryServiceField = internalTransfersExecutor.getClass().getSuperclass().getDeclaredField( "queryService");
        internalTransfersExecutorQueryServiceField.setAccessible(true);
        internalTransfersExecutorQueryServiceField.set(internalTransfersExecutor, queryService);

        Field internalTransfersExecutorCommandServiceField = internalTransfersExecutor.getClass().getSuperclass().getDeclaredField("commandService");
        internalTransfersExecutorCommandServiceField.setAccessible(true);
        internalTransfersExecutorCommandServiceField.set(internalTransfersExecutor, commandService);

        Field internalTransfersExecutorLoggerField = internalTransfersExecutor.getClass().getSuperclass().getDeclaredField("logger");
        internalTransfersExecutorLoggerField.setAccessible(true);
        internalTransfersExecutorLoggerField.set(internalTransfersExecutor, logger);
//        mockResource.generateFakeTransferOfType(entityManager, TransferType.Types.ELIXIR);
    }

//    @Test
    public void execute() throws SOAPException {
        Set<Transfer> transfers = queryService.getTransfersToExecute(TransferType.Types.ELIXIR);
        logger.info(Integer.toString(transfers.size()));
        Predicate<Transfer> filter = internalTransfersExecutor.getFilter();
        Set<Transfer> filteredTransfers = transfers.stream().filter(filter).collect(Collectors.toCollection(HashSet::new));

        int numbersOfTransfersShouldBeExecuted = filteredTransfers.size();
        logger.info(Integer.toString(numbersOfTransfersShouldBeExecuted));
        int numberOfExecutedTransfers = entityManager.createQuery("SELECT t FROM Transfer t WHERE t.status = :status").setParameter("status", Transfer.TransferStatuses.EXECUTED.getValue()).getResultList().size();
        logger.info(Integer.toString(numberOfExecutedTransfers));
        internalTransfersExecutor.execute();
        int numberOfExecutedTransferAfterExecute = entityManager.createQuery("SELECT t FROM Transfer t WHERE t.status = :status").setParameter("status", Transfer.TransferStatuses.EXECUTED.getValue()).getResultList().size();
        assertEquals(numberOfExecutedTransfers + numbersOfTransfersShouldBeExecuted, numberOfExecutedTransferAfterExecute);
    }

//    @Test
    public void getFilter() {
        Set<Transfer> transfers = queryService.getTransfersToExecute(TransferType.Types.ELIXIR);
        logger.info(Integer.toString(transfers.size()));
        Predicate<Transfer> filter = internalTransfersExecutor.getFilter();
        Set<Transfer> filteredTransfers = transfers.stream().filter(filter).collect(Collectors.toCollection(HashSet::new));
        logger.info(Integer.toString(filteredTransfers.size()));
        assertNotEquals(transfers.size(), filteredTransfers.size());
    }
}
