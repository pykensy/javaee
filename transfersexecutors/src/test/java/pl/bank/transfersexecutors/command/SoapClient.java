package pl.bank.transfersexecutors.command;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import pl.bank.shared.domain.model.transfer.StoreTransfers;
import pl.bank.shared.generators.IbanGenerator;
import pl.bank.shared.domain.model.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.*;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static pl.bank.shared.generators.mock.MockData.*;

public class SoapClient {
    private static Set<Transfer> elixirs = generateFakeTransferOfType(TransferType.Types.ELIXIR);

    public static void main(String[] args) {
        String soapEndpointUrl = "http://desktop-5md01gf:8080/bank/KirStoreImplService";
        String soapAction = "\"http://endpoint.kir.bank.pl/\"";

        callSoapWebService(soapEndpointUrl, soapAction);
    }

    private static void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException, NoSuchFieldException, InstantiationException, IllegalAccessException, JAXBException {
        soapMessage.setProperty(SOAPMessage.WRITE_XML_DECLARATION, "true");
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "ns2";
        String myNamespaceURI = "http://endpoint.kir.bank.pl/";

        SOAPEnvelope envelope = soapPart.getEnvelope();

        envelope.removeNamespaceDeclaration("SOAP-ENV");
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
        envelope.setPrefix("S");
        SOAPBody soapBody = envelope.getBody();
        soapBody.setPrefix("S");
        soapBody.addNamespaceDeclaration(myNamespace, myNamespaceURI);
        envelope.getHeader().removeNamespaceDeclaration("SOAP-ENV");
        String transfers = getMarshalledTransfers(elixirs, TransferType.Types.ELIXIR);
        transfers = transfers.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
        SOAPElement soapBodyElem = soapBody.addChildElement("storeElixirs", "ns2");
        SOAPElement arg0 = soapBodyElem.addChildElement("arg0");
        arg0.addTextNode(transfers);
        soapBodyElem.addChildElement(arg0);
    }

    private static void callSoapWebService(String soapEndpointUrl, String soapAction) {
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction), soapEndpointUrl);

            System.out.println("Response SOAP Message:");
            soapResponse.writeTo(System.out);
            System.out.println();
            Document document = soapResponse.getSOAPBody().extractContentAsDocument();
            Node firstChild = document.getDocumentElement().getFirstChild().getFirstChild();
            String value = firstChild.getNodeValue();

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
    }

    private static SOAPMessage createSOAPRequest(String soapAction) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();
        System.out.println(soapMessage);

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }

    private static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getMarshalledTransfers(Set<Transfer> transfers, TransferType.Types type) throws JAXBException, IllegalAccessException, NoSuchFieldException, InstantiationException {
        StringBuilder xml = new StringBuilder();

        Transfer[] tr = new Transfer[transfers.size()];

        JAXBContext jaxbContext = JAXBContext.newInstance(StoreTransfers.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(new StoreTransfers(transfers), stringWriter);

        return stringWriter.toString();
    }

    private static String getSoapAction(TransferType.Types type) {
        String result = null;
        switch (type) {
            case ELIXIR:
                result = "storeElixirs";
                break;
            case SORBNET:
                result = "storeSorbnets";
                break;
            case EXPRESS_ELIXIR:
                result = "storeExpressElixirs";
                break;
        }
        return result;
    }

    private static Set<Transfer> generateFakeTransferOfType(TransferType.Types type) {
        Set<Transfer> transfers = new HashSet<>();
        TransferType transferType = new TransferType(UUID.randomUUID(), type.getValue(), null, null);

        final Random random = new Random();

        final DecimalFormat df = new DecimalFormat("####0.00");

        double MIN_BALANCE = 0.0;
        double MAX_BALANCE = 10000000.0;

        for (int i = 0; i < 2; i++) {
            Transfer transfer = new Transfer();
            transfer.setId(UUID.randomUUID());
            transfer.setStatus(Transfer.TransferStatuses.EXECUTED.getValue());
            transfer.setType(transferType);

            transfer.setTransactionDate(LocalDate.now());

            transfer.setTitle(TITLES[Math.abs(random.nextInt()) % TITLES.length]);


            double amount = MIN_BALANCE + (random.nextDouble() * (MAX_BALANCE - MIN_BALANCE));

            transfer.setAmount(Double.valueOf(df.format(amount).replace(",", ".")));

            Beneficiary beneficiary = mockBeneficiary();
            beneficiary.setTransfer(transfer);
            transfer.setBeneficiary(beneficiary);
            User user = new User(UUID.randomUUID(), "dsfsdf", null, "dfsdf", "dsfsdfsd", "sdfsd", null);
            Account account = new Account(UUID.randomUUID(), IbanGenerator.generate(), "VIP", 123.45, user, null);
            transfer.setAccount(account);

            transfers.add(transfer);
        }
        return transfers;
    }

    private static Beneficiary mockBeneficiary() {
        final Random random = new Random();
        Beneficiary beneficiary = new Beneficiary();
        beneficiary.setId(UUID.randomUUID());

        String firstName = FIRST_NAMES[Math.abs(random.nextInt()) % FIRST_NAMES.length];
        String lastName = LAST_NAMES[Math.abs(random.nextInt()) % LAST_NAMES.length];
        beneficiary.setFullName(firstName + " " + lastName);

        beneficiary.setAccountIban(IbanGenerator.generate());

        beneficiary.setAddress(ADDRESSES[Math.abs(random.nextInt()) % ADDRESSES.length]);

        return beneficiary;
    }
}
