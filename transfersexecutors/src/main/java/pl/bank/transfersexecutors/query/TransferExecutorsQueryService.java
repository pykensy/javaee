package pl.bank.transfersexecutors.query;

import java.time.LocalDate;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Account;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Stateless
public class TransferExecutorsQueryService {

    @Inject
    private GenericDao dao;

    public Set<Transfer> getTransfersToExecute(TransferType.Types type) {
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("status", Transfer.TransferStatuses.SAVED.getValue());
        queryParams.put("transferType", type.getValue());

        return dao.executeNamedQuery(Transfer.GET_TRANSFERS_TO_EXECUTE, queryParams);
    }

    public Set<Transfer> filterTransfers(Set<Transfer> transfers, Predicate<Transfer> filter) {
        return transfers.stream()
                .filter(filter)
                .collect(Collectors.toCollection(HashSet::new));
    }

    public boolean isInternalBeneficiary(String accountIban) {
        return dao.findByProperty(Account.class, "iban", accountIban).size() == 1;
    }

    public boolean checkTransferSession(Transfer transfer) {
        LocalDate transferDate = transfer.getTransactionDate();
        LocalDate today = LocalDate.now();
        return transferDate.isEqual(today) || transferDate.isBefore(today);
    }
}
