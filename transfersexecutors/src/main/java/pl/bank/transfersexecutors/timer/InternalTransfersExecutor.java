package pl.bank.transfersexecutors.timer;

import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;

import javax.ejb.*;
import javax.xml.soap.SOAPException;
import java.util.Set;
import java.util.function.Predicate;

@LocalBean
@Stateless
@ApplicationException(rollback = true)
public class InternalTransfersExecutor extends TransferExecutor {

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedule(hour = "*", minute = "*/5")
    public void execute() throws SOAPException {
        logger.info("InternalTransfersExecutor running");
        commandService.generateInternalMockTransfers(TransferType.Types.ELIXIR);

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Set<Transfer> transfersToExecute = queryService.getTransfersToExecute(TransferType.Types.ELIXIR);

        Predicate<Transfer> filter = getFilter();

        transfersToExecute = queryService.filterTransfers(transfersToExecute, filter);

        logger.info(transfersToExecute.toString());

        transfersToExecute.forEach(transfer -> {
            commandService.updateAccountBalance(transfer);
            commandService.updateStatus(transfer);
        });
    }

    @Override
    protected Predicate<Transfer> getFilter() {
        return (transfer) ->
                queryService.isInternalBeneficiary(transfer.getBeneficiary().getAccountIban()) &&
                        queryService.checkTransferSession(transfer);
    }
}
