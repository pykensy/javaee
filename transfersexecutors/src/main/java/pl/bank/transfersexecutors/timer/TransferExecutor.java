package pl.bank.transfersexecutors.timer;

import org.slf4j.Logger;
import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;
import pl.bank.transfersexecutors.command.TransferExecutorsCommandService;
import pl.bank.transfersexecutors.query.TransferExecutorsQueryService;

import javax.inject.Inject;
import javax.xml.soap.SOAPException;
import java.io.Serializable;
import java.util.Set;
import java.util.function.Predicate;

public abstract class TransferExecutor implements Serializable {

    @Inject
    protected TransferExecutorsQueryService queryService;

    @Inject
    protected TransferExecutorsCommandService commandService;

    @Inject
    protected Logger logger;

    public void execute(TransferType.Types type) throws SOAPException {
        commandService.generateMockTransfers(type);

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Set<Transfer> transfersToExecute = queryService.getTransfersToExecute(type);

        Predicate<Transfer> filter = getFilter();

        transfersToExecute = queryService.filterTransfers(transfersToExecute, filter);

        logger.info(transfersToExecute.toString());

        transfersToExecute.forEach(transfer -> {
            commandService.updateStatus(transfer);
        });
        commandService.sendSOAP(transfersToExecute, type);
    }

    protected abstract Predicate<Transfer> getFilter();
}
