package pl.bank.transfersexecutors.timer;

import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;

import javax.ejb.*;
import javax.xml.soap.SOAPException;
import java.util.function.Predicate;

@LocalBean
@Stateless
public class ElixirTransfersExecutor extends TransferExecutor {

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedules({
            @Schedule(dayOfWeek = "Mon,Tue,Wed,Thu,Fri", hour = "8"),
            @Schedule(dayOfWeek = "Mon,Tue,Wed,Thu,Fri", hour = "12"),
            @Schedule(dayOfWeek = "Mon,Tue,Wed,Thu,Fri", hour = "16")
    })
    public void execute() throws SOAPException {
        logger.info("ElixirTransfersExecutor running");
        super.execute(TransferType.Types.ELIXIR);
    }

    @Override
    protected Predicate<Transfer> getFilter() {
        return (transfer) ->
                !queryService.isInternalBeneficiary(transfer.getBeneficiary().getAccountIban()) &&
                        queryService.checkTransferSession(transfer);

    }

}
