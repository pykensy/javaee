package pl.bank.transfersexecutors.timer;

import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;

import javax.ejb.*;
import javax.xml.soap.SOAPException;
import java.util.function.Predicate;

@LocalBean
@Stateless
public class ExpressElixirTransferExecutor extends TransferExecutor {

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedules({
            @Schedule(hour = "*")
    })
    public void execute() throws SOAPException {
        logger.info("ExpressElixirTransferExecutor running");
        super.execute(TransferType.Types.EXPRESS_ELIXIR);
    }

    @Override
    protected Predicate<Transfer> getFilter() {
        return (transfer) -> true;
    }
}
