package pl.bank.transfersexecutors.timer;

import pl.bank.shared.domain.model.Transfer;
import pl.bank.shared.domain.model.TransferType;

import javax.ejb.*;
import javax.xml.soap.SOAPException;
import java.util.function.Predicate;

@LocalBean
@Stateless
public class SorbnetTransferExecutor extends TransferExecutor {

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedule(hour = "*", minute = "*/10")
    public void execute() throws SOAPException {
        logger.info("SorbnetTransferExecutor running");
        super.execute(TransferType.Types.SORBNET);
    }

    @Override
    protected Predicate<Transfer> getFilter() {
        return (transfer) -> true;
    }
}
