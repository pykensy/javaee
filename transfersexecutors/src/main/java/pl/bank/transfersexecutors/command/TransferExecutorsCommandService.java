package pl.bank.transfersexecutors.command;

import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.*;
import pl.bank.shared.generators.IbanGenerator;
import pl.bank.shared.generators.mock.AccountsGenerator;
import pl.bank.shared.generators.mock.BeneficiariesGenerator;
import pl.bank.shared.generators.mock.TransfersGenerator;
import pl.bank.shared.generators.mock.UsersGenerator;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.soap.SOAPException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static pl.bank.shared.generators.mock.MockData.*;


@Stateless
public class TransferExecutorsCommandService {

    @Inject
    private GenericDao dao;

    @Inject
    private TransferOutputSoapService soapService;

    public void updateAccountBalance(Transfer transfer) {
        Account account = transfer.getAccount();
        Double newBalance = account.getBalance() + transfer.getAmount();
        account.setBalance(newBalance);
        dao.merge(account);
    }

    public void updateStatus(Transfer transfer) {
        transfer.setStatus(Transfer.TransferStatuses.EXECUTED.getValue());
        dao.merge(transfer);
    }

    public void sendSOAP(Set<Transfer> transfers, TransferType.Types type) throws SOAPException {
        transfers.forEach(System.out::println);
        soapService.sendTransfers(transfers, type);
    }

    public void generateMockTransfers(TransferType.Types type) {
        int numberOfMockedTransfers = 100;

        Set<Transfer> transfers = new HashSet<>();
//        TransferType transferType = new TransferType(UUID.randomUUID(), type.getValue(), null, null);
        TransferType transferType = dao.findByProperty(TransferType.class, "name", type.getValue()).stream().findFirst().get();
        Set<Account> accounts = dao.findAll(Account.class);
        Random random = new Random();

        for (int i = 0; i < numberOfMockedTransfers; i++) {
            Transfer transfer = TransfersGenerator.generateTransfer(Transfer.TransferDirect.OUTGOING);
            transfer.setStatus(Transfer.TransferStatuses.EXECUTED.getValue());
            transfer.setType(transferType);

            transfer.setTransactionDate(LocalDate.now());

            Beneficiary beneficiary = BeneficiariesGenerator.generateBeneficiary();
            beneficiary.setTransfer(transfer);
            transfer.setBeneficiary(beneficiary);
            Account account = accounts.stream().skip(Math.abs(random.nextInt()) % accounts.size()).findFirst().get();
            transfer.setAccount(account);

            transfers.add(transfer);
        }

        transfers.forEach(dao::merge);
    }

    public void generateInternalMockTransfers(TransferType.Types type) {
        int numberOfMockedTransfers = 100;

        Set<Transfer> transfers = new HashSet<>();
//        TransferType transferType = new TransferType(UUID.randomUUID(), type.getValue(), null, null);
        TransferType transferType = dao.findByProperty(TransferType.class, "name", type.getValue()).stream().findFirst().get();
        Set<Account> accounts = dao.findAll(Account.class);
        Random random = new Random();

        for (int i = 0; i < numberOfMockedTransfers; i++) {
            Transfer transfer = TransfersGenerator.generateTransfer(Transfer.TransferDirect.OUTGOING);
            transfer.setStatus(Transfer.TransferStatuses.EXECUTED.getValue());
            transfer.setType(transferType);

            transfer.setTransactionDate(LocalDate.now());
            Account beneficiaryAccount = accounts.stream().skip(Math.abs(random.nextInt()) % accounts.size()).findFirst().get();
            Beneficiary beneficiary = new Beneficiary();
            beneficiary.setAccountIban(beneficiaryAccount.getIban());
            beneficiary.setFullName(beneficiaryAccount.getUser().getFirstName() + " " + beneficiaryAccount.getUser().getLastName());
            beneficiary.setId(UUID.randomUUID());
            beneficiary.setAddress("Adres");
            beneficiary.setTransfer(transfer);
            transfer.setBeneficiary(beneficiary);
            Account account = accounts.stream().skip(Math.abs(random.nextInt()) % accounts.size()).findFirst().get();
            transfer.setAccount(account);

            transfers.add(transfer);
        }

        transfers.forEach(dao::save);
    }
}
