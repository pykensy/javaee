package pl.bank.accounts.resource;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bank.config.MockResource;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Account;
import pl.bank.shared.domain.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import static pl.bank.config.MockResource.NUMBER_OF_ACCOUNTS_PER_USER;
import static pl.bank.shared.generators.mock.MockData.DEFAULT_USER_PASSWORD;

@RunWith(Arquillian.class)
public class UserAccountsResourceTest {

    private final String URL = "http://localhost:8080/bank/api/";

    private static WebTarget clientGetUserAccounts;
    private static WebTarget loginClient;

    private GenericDao dao;

    private Logger logger = LoggerFactory.getLogger(UserAccountsResourceTest.class);

    @ArquillianResource
    private URL base;

    private String token;

    private User user;

    private EntityManager entityManager;

    private Set<User> users = new HashSet<>();

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, "pl.bank")
                .addAsResource("META-INF/beans.xml");
    }

    @Before
    public void setUp() throws MalformedURLException, NoSuchFieldException, IllegalAccessException {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("h2");
        entityManager = entityManagerFactory.createEntityManager();

        MockResource mockResource = MockResource.getInstance();
//        mockResource.setEntityManager(entityManager);
//        mockResource.mockDatabaseData();

        dao = new GenericDao();
        Field daoEmField = GenericDao.class.getDeclaredField("entityManager");
        daoEmField.setAccessible(true);
        daoEmField.set(dao, entityManager);

        loginClient = ClientBuilder.newClient().target(new URL(base, "api/auth/token").toExternalForm());

        users = dao.findAll(User.class);

        user = users.stream().findFirst().get();

        token = login(user.getUsername(), DEFAULT_USER_PASSWORD);
    }

    @InSequence(0)
    @RunAsClient
    @Test
    public void getUserAccounts() throws MalformedURLException {
        clientGetUserAccounts = ClientBuilder
                .newClient()
                .target(new URL(base, "api/accounts/user/" + user.getId().toString())
                        .toExternalForm());

        String authHeader = "Bearer " + token;

        Set<Account> result = (Set<Account>) clientGetUserAccounts
                .request()
                .header("Authorization", authHeader)
                .get(Set.class);

        logger.info(result.toString());
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), NUMBER_OF_ACCOUNTS_PER_USER);
    }

    private String login(String username, String password) {
        Form form = new Form();
        form
                .param("username", username)
                .param("password", password);

        Response authResponse = loginClient
                .request(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.form(form));
        return authResponse.readEntity(String.class);
    }

}
