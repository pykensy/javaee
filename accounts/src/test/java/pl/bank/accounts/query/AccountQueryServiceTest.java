package pl.bank.accounts.query;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.bank.config.MockResource;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Account;
import pl.bank.shared.domain.model.User;
import pl.bank.shared.generators.mock.AccountsGenerator;
import pl.bank.shared.generators.mock.UsersGenerator;

import javax.inject.Inject;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class AccountQueryServiceTest {

    private User user;

    @Inject
    private GenericDao dao;

    @Inject
    private AccountQueryService accountQueryService;

    private final static int NUMBER_OF_USER_ACCOUNTS = 5;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, Filters.exclude(MockResource.class), "pl.bank")
                .addAsResource("META-INF/beans.xml");
    }

    @Before
    public void setUp() throws Exception {
        UUID userId = dao.save(UsersGenerator.generateUser());
        user = dao.find(User.class, userId).get();

        Set<Account> accounts = AccountsGenerator.generateAccounts(NUMBER_OF_USER_ACCOUNTS);
        accounts.forEach(account -> {
            account.setUser(user);
            dao.save(account);
        });
    }

    @Test
    public void getUserAccounts() {
        Set<Account> accounts = accountQueryService.getUserAccounts(user.getId());
        assertEquals(accounts.size(), NUMBER_OF_USER_ACCOUNTS);
    }

    @Test
    public void getAccount() {
        Account account = accountQueryService.getUserAccounts(user.getId()).stream().findFirst().get();
        Account accountShouldBeFinded = accountQueryService.getAccount(account.getId());
        assertEquals(account, accountShouldBeFinded);
    }
}
