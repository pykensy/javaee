package pl.bank.accounts.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import pl.bank.accounts.query.AccountQueryService;
import pl.bank.security.filters.JWTTokenNeeded;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

import static pl.bank.config.api.ApplicationConfig.*;

@Path("accounts")
@Stateless
public class AccountResource {

    @Inject
    private Logger logger;

    @Inject
    private AccountQueryService accountQueryService;

    @JWTTokenNeeded(roles = {ADMIN_ROLE, EMPLOYEE_ROLE, USER_ROLE})
    @GET
    @Path("/user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserAccounts(@PathParam("id") UUID id) throws JsonProcessingException {
        logger.info(id.toString());
        ObjectMapper mapper = new ObjectMapper();
        String resultJson = mapper.writeValueAsString(accountQueryService.getUserAccounts(id));
        return Response.ok().entity(resultJson).build();
    }

    @JWTTokenNeeded(roles = {ADMIN_ROLE, EMPLOYEE_ROLE, USER_ROLE})
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccountDetails(@PathParam("id") UUID id) throws JsonProcessingException {
        logger.info(id.toString());
        ObjectMapper mapper = new ObjectMapper();
        String resultJson = mapper.writeValueAsString(accountQueryService.getAccount(id));
        return Response.ok().entity(resultJson).build();
    }
}
