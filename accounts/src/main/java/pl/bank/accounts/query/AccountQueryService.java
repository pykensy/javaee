package pl.bank.accounts.query;

import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.Account;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.*;

@Stateless
public class AccountQueryService {

    @Inject
    private GenericDao dao;

    public Set<Account> getUserAccounts(UUID id) {
        Map<String, Object> queryParameters = new HashMap<>(1);
        queryParameters.put("id", id);
        return dao.executeNamedQuery(Account.FIND_USER_ACCOUNTS, queryParameters);
    }


    public Account getAccount(UUID id) {
        return dao.find(Account.class, id).orElseThrow(EntityNotFoundException::new);
    }
}
