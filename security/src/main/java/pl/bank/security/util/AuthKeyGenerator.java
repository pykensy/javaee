package pl.bank.security.util;

import pl.bank.security.config.SecurityConfig;

import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import java.security.Key;

@ApplicationScoped
public class AuthKeyGenerator implements KeyGenerator {
    @Produces
    @Override
    public Key generateKey() {
        Key key = new SecretKeySpec(SecurityConfig.API_KEY.getBytes(), 0, SecurityConfig.API_KEY.getBytes().length, "DES");
        return key;
    }
}
