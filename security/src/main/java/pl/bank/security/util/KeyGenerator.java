package pl.bank.security.util;

import java.security.Key;

public interface KeyGenerator {
    Key generateKey();
}
