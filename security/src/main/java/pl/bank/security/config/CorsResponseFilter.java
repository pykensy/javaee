package pl.bank.security.config;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.util.List;

@Provider
public class CorsResponseFilter implements ContainerResponseFilter {
    public static final String ALLOWED_METHODS = "GET, POST, PUT, DELETE, OPTIONS, HEAD";
    public static final int MAX_AGE = 151200;
    public static final String DEFAULT_ALLOWED_HEADERS = "origin,accept,content-type";

    public CorsResponseFilter() {
    }

    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        MultivaluedMap<String, Object> headers = responseContext.getHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Headers", this.getRequestedHeaders(requestContext));
        headers.add("Access-Control-Allow-Credentials", "true");
        headers.add("Access-Control-Allow-Methods", ALLOWED_METHODS);
        headers.add("Access-Control-Max-Age", MAX_AGE);
        headers.add("x-responded-by", "cors-response-filter");
    }

    private String getRequestedHeaders(ContainerRequestContext responseContext) {
        List<String> headers = responseContext.getHeaders().get("Access-Control-Request-Headers");
        return this.createHeaderList(headers);
    }

    private String createHeaderList(List<String> headers) {
        if (headers != null && !headers.isEmpty()) {
            StringBuilder retVal = new StringBuilder();

            for (String header : headers) {
                retVal.append(header);
                retVal.append(',');
            }

            retVal.append("origin,accept,content-type");
            return retVal.toString();
        } else {
            return "origin,accept,content-type";
        }
    }
}
