package pl.bank.security.query;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import pl.bank.security.util.KeyGenerator;
import pl.bank.shared.utils.PasswordUtils;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Stateless
public class SecurityQueryService {

    @Inject
    private Logger logger;

    @Inject
    private GenericDao dao;

    @Inject
    private KeyGenerator keyGenerator;

    @Context
    private UriInfo uriInfo;

    public User getAuthenticatedUser(String username, String password) throws SecurityException {

        Map<String, Object> queryParameters = new HashMap<>(2);
        queryParameters.put("username", username);
        queryParameters.put("password", PasswordUtils.digestPassword(password));

        Set<User> user = dao.executeNamedQuery(User.FIND_USER_BY_USERNAME_AND_PASSWORD, queryParameters);

        if (user.size() != 1) {
            throw new SecurityException("Invalid user/password");
        }

        return (User)user.toArray()[0];
    }

    public String getToken(User user) {

        Map<String, Object> claims = new HashMap<>(1);
        claims.put("user", user);
        Key key = keyGenerator.generateKey();
        String jwtToken = Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getUsername())
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
        logger.info("#### generating token for a key : " + jwtToken + " - " + key);
        return jwtToken;

    }

    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public Optional<User> findUserById(UUID id) {
        return dao.find(User.class, id);
    }
}
