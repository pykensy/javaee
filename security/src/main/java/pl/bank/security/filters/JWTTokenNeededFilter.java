package pl.bank.security.filters;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import pl.bank.security.query.SecurityQueryService;
import pl.bank.security.util.KeyGenerator;
import pl.bank.shared.domain.model.User;

import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.security.Key;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Provider
@JWTTokenNeeded
@Priority(Priorities.AUTHENTICATION)
public class JWTTokenNeededFilter implements ContainerRequestFilter {

    @Inject
    private Logger logger;

    @Inject
    private KeyGenerator keyGenerator;

    @Context
    private ResourceInfo resourceInfo;

    @EJB
    private SecurityQueryService securityQueryService;

    @SuppressWarnings("unchecked")
    @Override
    public void filter(ContainerRequestContext requestContext) {

        Method method = resourceInfo.getResourceMethod();
        JWTTokenNeeded jwtTokenNeededAnnotation = method.getAnnotation(JWTTokenNeeded.class);

        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        logger.info("#### authorizationHeader : " + authorizationHeader);

        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            logger.info("#### invalid authorizationHeader : " + authorizationHeader);
            throw new NotAuthorizedException("Authorization header must be provided");
        }

        String token = authorizationHeader.substring("Bearer".length()).trim();
        token = token.replace("\"", "");
        try {
            Key key = keyGenerator.generateKey();

            Jws<Claims> claims = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token);
            Map<String, Object> userMap = (Map<String, Object>)claims.getBody().get("user");

            Optional<User> optionalUser = securityQueryService.findUserById((UUID.fromString((String)userMap.get("id"))));
            if (optionalUser.isPresent()) {
                if (Arrays.stream(jwtTokenNeededAnnotation.roles()).noneMatch(role -> role.equals(optionalUser.get().getRole()))) {
                    throw new Exception("Invalid role");
                }
            } else {
                throw new Exception("Invalid user");
            }

            Jwts.parser().setSigningKey(key).parseClaimsJws(token);
            logger.info("#### valid token : " + token);
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn("#### invalid token : " + token);
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }
}
