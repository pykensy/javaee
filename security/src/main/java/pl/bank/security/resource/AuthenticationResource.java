package pl.bank.security.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import pl.bank.security.query.SecurityQueryService;
import pl.bank.shared.domain.model.User;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Path("/auth")
@Stateless
public class AuthenticationResource {

    @Inject
    private Logger logger;

    @Inject
    private SecurityQueryService securityQueryService;

    @Resource
    private ManagedExecutorService mes;

    @POST
    @Path("/token")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response token(@FormParam("username") String username,
                          @FormParam("password") String password) {
        logger.info(username + " " + password);

        try {
            User user = securityQueryService.getAuthenticatedUser(username, password);
            String token = securityQueryService.getToken(user);

            ObjectMapper mapper = new ObjectMapper();
            String resultJson = mapper.writeValueAsString(token);

            return Response.ok().entity(resultJson).build();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString());
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

}
