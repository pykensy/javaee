package pl.bank.security.query;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;
import pl.bank.config.MockResource;
import pl.bank.security.util.KeyGenerator;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.User;
import pl.bank.shared.generators.mock.UsersGenerator;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.core.UriInfo;
import java.lang.reflect.Field;
import java.net.URI;
import java.security.Key;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doThrow;
import static pl.bank.shared.generators.mock.MockData.DEFAULT_USER_PASSWORD;

@RunWith(Arquillian.class)
public class SecurityQueryServiceTest {

    private SecurityQueryService securityQueryService;

    @EJB
    private GenericDao dao;

    @Inject
    private KeyGenerator keyGenerator;


    private User user;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, Filters.exclude(MockResource.class), "pl.bank")
                .addAsResource("META-INF/beans.xml");
    }

    @Before
    public void setUp() throws Exception {
        securityQueryService = new SecurityQueryService();
        Field loggerField = SecurityQueryService.class.getDeclaredField("logger");
        loggerField.setAccessible(true);
        loggerField.set(securityQueryService, LoggerFactory.getLogger(SecurityQueryService.class));

        Field daoField = SecurityQueryService.class.getDeclaredField("dao");
        daoField.setAccessible(true);
        daoField.set(securityQueryService, dao);

        Field keyField = SecurityQueryService.class.getDeclaredField("keyGenerator");
        keyField.setAccessible(true);
        keyField.set(securityQueryService, keyGenerator);

        UriInfo uriInfo = Mockito.mock(UriInfo.class);
        Mockito.when(uriInfo.getAbsolutePath())
                .thenReturn(URI.create("http://localhost:8080/bank/api"));
        Field uriInfoField = SecurityQueryService.class.getDeclaredField("uriInfo");
        uriInfoField.setAccessible(true);
        uriInfoField.set(securityQueryService, uriInfo);

        User newUser = UsersGenerator.generateUser();
        dao.save(newUser);
        user = dao.find(User.class, newUser.getId()).get();
    }

    @Test
    public void getAuthenticatedUser() {
        User authUser = securityQueryService.getAuthenticatedUser(user.getUsername(), DEFAULT_USER_PASSWORD);
        assertEquals(authUser, user);
    }

    @Test
    public void getToken() {
        String token = securityQueryService.getToken(user);
        assertNotNull(token);

        Key key = keyGenerator.generateKey();

        Jws<Claims> claims = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token);
        Map<String, Object> userMap = (Map<String, Object>)claims.getBody().get("user");
        Optional<User> optionalUser = securityQueryService.findUserById((UUID.fromString((String)userMap.get("id"))));

        assertEquals(optionalUser.get(), user);

        Jwts.parser().setSigningKey(key).parseClaimsJws(token);
    }

    @Test
    public void findUserByIdShouldBeSuccess() {
        User searchesUser = securityQueryService.findUserById(user.getId()).get();
        assertEquals(searchesUser, user);
    }
}
