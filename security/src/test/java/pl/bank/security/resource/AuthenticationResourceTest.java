package pl.bank.security.resource;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;
import pl.bank.config.MockResource;
import pl.bank.security.query.SecurityQueryService;
import pl.bank.security.util.AuthKeyGenerator;
import pl.bank.security.util.KeyGenerator;
import pl.bank.shared.dao.GenericDao;
import pl.bank.shared.domain.model.User;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URL;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;
import static pl.bank.shared.generators.mock.MockData.DEFAULT_USER_PASSWORD;

@RunWith(Arquillian.class)
public class AuthenticationResourceTest {

    private AuthenticationResource authenticationResource;

    private GenericDao dao;

    private MockResource mockResource;

    private Set<User> users;

    private static WebTarget loginClient;

    @Mock
    private SecurityQueryService securityQueryService;

    private EntityManager entityManager;

    @Resource
    private ManagedExecutorService mes;

    @ArquillianResource
    private URL base;

    private User user;

    @Mock
    private UriInfo uriInfo;

    private KeyGenerator keyGenerator = new AuthKeyGenerator();

    private Key key;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addClass(MockResource.class)
                .addPackages(true, "pl.bank")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Before
    public void setUp() throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("h2");
        entityManager = entityManagerFactory.createEntityManager();

        mockResource = MockResource.getInstance();
//        mockResource.setEntityManager(entityManager);
//        mockResource.mockDatabaseData();

        dao = new GenericDao();
        Field emDaoField = GenericDao.class.getDeclaredField("entityManager");
        emDaoField.setAccessible(true);
        emDaoField.set(dao, entityManager);

        users = dao.findAll(User.class);

        authenticationResource = new AuthenticationResource();
        Field loggerField = AuthenticationResource.class.getDeclaredField("logger");
        Field securityQueryServiceField = AuthenticationResource.class.getDeclaredField("securityQueryService");
        Field mesField = AuthenticationResource.class.getDeclaredField("mes");

        loggerField.setAccessible(true);
        securityQueryServiceField.setAccessible(true);
        mesField.setAccessible(true);

        user = users.stream().findFirst().get();

        securityQueryService = Mockito.mock(SecurityQueryService.class);

        when(securityQueryService.findUserById(anyObject())).thenReturn(Optional.of(user));
        when(securityQueryService.getAuthenticatedUser(user.getUsername(), DEFAULT_USER_PASSWORD)).thenReturn(user);
        String token = getToken();
        when(securityQueryService.getToken(anyObject())).thenReturn(token);

        loggerField.set(authenticationResource, LoggerFactory.getLogger(AuthenticationResource.class));
        securityQueryServiceField.set(authenticationResource, securityQueryService);

        mesField.set(authenticationResource, mes);

        loginClient = ClientBuilder.newClient().target(new URL(base, "api/auth/token").toExternalForm());
    }

    private String getToken() {
        uriInfo = Mockito.mock(UriInfo.class);
        Mockito.when(uriInfo.getAbsolutePath())
                .thenReturn(URI.create("http://localhost:8080/bank/api"));

        Map<String, Object> claims = new HashMap<>(1);
        claims.put("user", user);
        key = keyGenerator.generateKey();
        String jwtToken = Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getUsername())
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
        return jwtToken;
    }

    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    @InSequence(1)
    @Test
    public void getTokenWithCMethodCall() throws InterruptedException {
        User user = users.stream().findFirst().get();
        String token = (String) authenticationResource.token(user.getUsername(), DEFAULT_USER_PASSWORD).getEntity();
        Assert.assertNotNull(token);
    }

    @InSequence(2)
    @Test
    public void getTokenForEveryUserInSameTime() {
        ExecutorService service = Executors.newFixedThreadPool(users.size());
        users.forEach(user -> {
            service.submit(() -> {
                String token = (String) authenticationResource.token(user.getUsername(), DEFAULT_USER_PASSWORD).getEntity();
                Assert.assertNotNull(token);
            });
        });

        service.shutdown();
    }
}
